#pragma once

#include <jni.h>
#include <map>
#include "converters.h"


// Adapted from https://gist.github.com/theeasiestway/e5f453715cecc55b5ca57d0628b9f12a

namespace KlassHashMap
{
    inline static jmethodID entrySet_;
    inline static jmethodID iterator_;
    inline static jmethodID hasNext_;
    inline static jmethodID next_;
    inline static jmethodID getKey_;
    inline static jmethodID getValue_;
    inline static jmethodID intValue_;

    template<class K>
    K get_key(JNIEnv* env, jobject entry)
    {
        return from_java<K>(env, env->CallObjectMethod(entry, getKey_));
    }

    template<>
    unsigned get_key(JNIEnv* env, jobject entry)
    {
        jobject key = env->CallObjectMethod(entry, getKey_);
        int     val = env->CallIntMethod(key, intValue_);
        return static_cast<unsigned>(val);
    }

    template<class V>
    V get_value(JNIEnv* env, jobject entry)
    {
        return from_java<V>(env, env->CallObjectMethod(entry, getValue_));
    }

    template<class K, class V>
    std::pair<K, V> get_entry(JNIEnv* env, jobject iter)
    {
        jobject entry = env->CallObjectMethod(iter, next_);

        K key = get_key<K>(env, entry);
        V val = get_value<V>(env, entry);

        return std::make_pair(key, val);
    }

    void init(JNIEnv* env)
    {
        jclass hashMapClass = env->FindClass("java/util/Map");
        entrySet_ = env->GetMethodID(hashMapClass, "entrySet", "()Ljava/util/Set;");
        env->DeleteLocalRef(hashMapClass);

        jclass setClass = env->FindClass("java/util/Set");
        iterator_ = env->GetMethodID(setClass, "iterator", "()Ljava/util/Iterator;");
        env->DeleteLocalRef(setClass);

        jclass iteratorClass = env->FindClass("java/util/Iterator");
        hasNext_ = env->GetMethodID(iteratorClass, "hasNext", "()Z");
        next_    = env->GetMethodID(iteratorClass, "next", "()Ljava/lang/Object;");
        env->DeleteLocalRef(iteratorClass);

        jclass entryClass = env->FindClass("java/util/Map$Entry");
        getKey_   = env->GetMethodID(entryClass, "getKey", "()Ljava/lang/Object;");
        getValue_ = env->GetMethodID(entryClass, "getValue", "()Ljava/lang/Object;");
        env->DeleteLocalRef(entryClass);

        jclass integerClass = env->FindClass("java/lang/Integer");
        intValue_ = env->GetMethodID(integerClass, "intValue", "()I");
        env->DeleteLocalRef(integerClass);
    }

    template<class K, class V>
    std::map<K, V> from_java(JNIEnv* env, const jobject obj)
    {
        std::map<K, V> dest;

        jobject set  = env->CallObjectMethod(obj, entrySet_);
        jobject iter = env->CallObjectMethod(set, iterator_);

        while (env->CallBooleanMethod(iter, hasNext_))
            dest.insert(get_entry<K, V>(env, iter));

        env->DeleteLocalRef(iter);
        env->DeleteLocalRef(set);

        return dest;
    }
}


// Note: partial function template specialization is not supported by C++. This is a simple workaround.
template<>
std::map<BodyState, MetabolicParams> from_java(JNIEnv* env, const jobject obj)
{
    return KlassHashMap::from_java<BodyState, MetabolicParams>(env, obj);
}

template<>
std::map<unsigned, ExerciseType> from_java(JNIEnv* env, const jobject obj)
{
    return KlassHashMap::from_java<unsigned, ExerciseType>(env, obj);
}

template<>
std::map<unsigned, FoodType> from_java(JNIEnv* env, const jobject obj)
{
    return KlassHashMap::from_java<unsigned, FoodType>(env, obj);
}
