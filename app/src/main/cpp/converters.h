#pragma once

#include <jni.h>
#include <functional>  // std::invoke
#include <map>
#include <string>
#include <type_traits>
#include <utility>     // std::move
#include "carbmetlib/source/SimCtl.h"


// ---------------------------------------------------------------------------------------------------------------------
// Notes
// ---------------------------------------------------------------------------------------------------------------------
/*

For ease of maintenance, I've automated the logic of getters and setters for fieldIDs. I tried several strategies with
the main goal of having a single point of definition for each field. I tried storing in vectors both pointers to members
and lambdas calling them, but in either case could not unify the different types successfully. I did not want to use
std::function since that carries a runtime overhead. Finally, I fell back on using macros.
-- Mike

Example:

FIELD_CONVERTER(HaltEvent,
    FIELD(fireTime,  "I"),
    FIELD(eventType, "Ledu/uwm/cs/cs595/carbmetsimapp/EventType")
)

expands to

extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_HaltEvent_00024Companion_onClassInit(
    JNIEnv *env, jobject thiz
){
    using Clazz = HaltEvent;
    Klass<HaltEvent>::init(env, "edu/uwm/cs/cs595/carbmetsimapp/" "HaltEvent",
        FieldInfo{"fireTime", "I", &Clazz::fireTime},
        FieldInfo{"eventType", "Ledu/uwm/cs/cs595/carbmetsimapp/EventType", &Clazz::eventType}
    );
}


Signatures:
The following command will print the signatures of all public member functions of a java class:
javap -s -public classpath

Example:
javap -s -public java.util.ArrayList

public class java.util.ArrayList<E> extends java.util.AbstractList<E>
    implements java.util.List<E>, java.util.RandomAccess, java.lang.Cloneable, java.io.Serializable {
  public java.util.ArrayList(int);
    descriptor: (I)V

  public java.util.ArrayList();
    descriptor: ()V
...
{


jobject references:
Because all get_field and set_field functions are called in a loop, it is important to delete all local references.

*/


// ---------------------------------------------------------------------------------------------------------------------
// Base
// ---------------------------------------------------------------------------------------------------------------------
template<class M>
struct FieldInfo
{
    FieldInfo(const char* name, const char* sig, M member_ptr)
        : name{name}, sig{sig}, member_ptr{member_ptr}
    {}

    const char* name;
    const char* sig;
    M           member_ptr;
};

template<class T>
struct EnumInfo
{
    EnumInfo(const char* name, T value)
        : name{name}, value{value}
    {}

    const char* name;
    T           value;
};

template<class CppType>
using IsInputType = std::disjunction<
    std::is_same<CppType, BloodParams>,
    std::is_same<CppType, BrainParams>,
    std::is_same<CppType, HeartParams>,
    std::is_same<CppType, HumanParams>,
    std::is_same<CppType, IntestineParams>,
    std::is_same<CppType, KidneysParams>,
    std::is_same<CppType, LiverParams>,
    std::is_same<CppType, MusclesParams>,
    std::is_same<CppType, PortalVeinParams>,
    std::is_same<CppType, StomachParams>
>;

template<class CppType>
using RequireInput = std::enable_if_t<IsInputType<CppType>::value,bool>;

template<class CppType>
using RequireEnum = std::enable_if_t<std::is_enum_v<CppType>, bool>;

template<class CppType>
using RequireNotEnum = std::enable_if_t<!std::is_enum_v<CppType>, bool>;

template<class CppType>
using RequireString = std::enable_if_t<std::is_same_v<CppType, std::string>, bool>;


template<class CppType, typename Enable = void, class... M>
struct KlassImpl;

template<class CppType, class... M>
KlassImpl<CppType, void, M...> get_Klass_instance(FieldInfo<M>&&...)
{
    return {};
}

template<class CppType, class... T>
KlassImpl<CppType, void, T...> get_Klass_instance(EnumInfo<T>&&...)
{
    return {};
}

template<class CppType> struct KlassInfo {};

template<class CppType>
using Klass = typename KlassInfo<CppType>::Type;


#define FIELD_CONVERTER(NAME, ...)                                                \
template<>                                                                        \
struct KlassInfo<NAME>                                                            \
{                                                                                 \
    using Clazz = NAME;                                                           \
    using Type = decltype(get_Klass_instance<NAME>(__VA_ARGS__));                 \
};                                                                                \
                                                                                  \
extern "C"                                                                        \
JNIEXPORT void JNICALL                                                            \
Java_edu_uwm_cs_cs595_carbmetsimapp_##NAME##_00024Companion_onClassInit(          \
    JNIEnv *env, jobject thiz                                                     \
){                                                                                \
    using Clazz = NAME;                                                           \
    Klass<NAME>::init(env, "edu/uwm/cs/cs595/carbmetsimapp/" #NAME, __VA_ARGS__); \
}

#define FIELD(NAME, SIG)    FieldInfo{#NAME, SIG, &Clazz::NAME}

#define ENUM_CONVERTER(NAME, ...)    FIELD_CONVERTER(NAME, __VA_ARGS__)
#define ENUM_MEMBER(NAME)            EnumInfo{#NAME, Clazz::NAME}


// ---------------------------------------------------------------------------------------------------------------------
// Primitives
// ---------------------------------------------------------------------------------------------------------------------
void get_field(JNIEnv* env, const jobject obj, jfieldID id, bool& dest)
{
    dest = static_cast<bool>(env->GetBooleanField(obj, id));
}

void set_field(const bool& val, JNIEnv* env, jobject obj, jfieldID id)
{
    env->SetBooleanField(obj, id, static_cast<jboolean>(val));
}


void get_field(JNIEnv* env, const jobject obj, jfieldID id, int& dest)
{
    dest = static_cast<int>(env->GetIntField(obj, id));
}

void set_field(const int& val, JNIEnv* env, const jobject obj, jfieldID id)
{
    env->SetIntField(obj, id, static_cast<jint>(val));
}


// Warning: kludge. Java doesn't have an unsigned integer type, so we're using Int. This only works because there are
// no signed int in carbmetsim.
void get_field(JNIEnv* env, const jobject obj, jfieldID id, unsigned& dest)
{
    dest = static_cast<unsigned>(env->GetIntField(obj, id));
}

void set_field(const unsigned& val, JNIEnv* env, jobject obj, jfieldID id)
{
    env->SetIntField(obj, id, static_cast<jint>(val));
}


void get_field(JNIEnv* env, const jobject obj, jfieldID id, double& dest)
{
    dest = static_cast<double>(env->GetDoubleField(obj, id));
}

void set_field(const double& val, JNIEnv* env, const jobject obj, jfieldID id)
{
    env->SetDoubleField(obj, id, static_cast<jdouble>(val));
}


// ---------------------------------------------------------------------------------------------------------------------
// String
// ---------------------------------------------------------------------------------------------------------------------
// https://docs.oracle.com/en/java/javase/15/docs/specs/jni/functions.html#string-operations
void copy_from_java(JNIEnv* env, const jobject src, std::string& dest)
{
    jstring val = static_cast<jstring>(src);

    // java buffers are not required to be null terminated
    jsize size = env->GetStringUTFLength(val) + 1;

    dest.clear();
    dest.reserve(size);

    env->GetStringUTFRegion(val, 0, size - 1, dest.data());
    dest[size] = '\0';
}


// ---------------------------------------------------------------------------------------------------------------------
// Arrays
// ---------------------------------------------------------------------------------------------------------------------
template<class T>
struct CppArrayView
{
    const T* buf;
    int      size;

    CppArrayView(const T* buf, int size)
        : buf{buf}, size{size}
    {}
};

void set_field(const CppArrayView<double>& src, JNIEnv* env, jobject obj, jfieldID id)
{
    jdoubleArray dest = static_cast<jdoubleArray>(env->GetObjectField(obj, id));
    env->SetDoubleArrayRegion(dest, 0, src.size, src.buf);
    env->DeleteLocalRef(dest);
}

void set_field(const CppArrayView<int>& src, JNIEnv* env, jobject obj, jfieldID id)
{
    jintArray dest = static_cast<jintArray>(env->GetObjectField(obj, id));
    env->SetIntArrayRegion(dest, 0, src.size, src.buf);
    env->DeleteLocalRef(dest);
}

template<auto member_ptr, class CppType, int size>
auto make_CppArrayView(const CppType& klass)
{
    return CppArrayView{std::invoke(member_ptr, klass), size};
}

#define ARRAYFIELD(NAME, SIG, SIZE)    FieldInfo{#NAME, SIG, &make_CppArrayView<&Clazz::NAME, Clazz, SIZE>}


// ---------------------------------------------------------------------------------------------------------------------
// ArrayList
// ---------------------------------------------------------------------------------------------------------------------
struct KlassArrayList
{
    inline static jmethodID add_;
    inline static jmethodID arrayList_;

    inline static jclass arrayListClass;

    static void init(JNIEnv* env)
    {
        arrayListClass = reinterpret_cast<jclass>(env->NewGlobalRef(env->FindClass("java/util/ArrayList")));

        add_       = env->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");
        arrayList_ = env->GetMethodID(arrayListClass, "<init>", "(I)V");
    }

    static void destruct(JNIEnv* env)
    {
        env->DeleteGlobalRef(arrayListClass);
    }
};


// ---------------------------------------------------------------------------------------------------------------------
// Record types
// ---------------------------------------------------------------------------------------------------------------------
template<class CppType, class... M>
struct KlassImpl<CppType, std::enable_if_t<!std::is_enum_v<CppType>>, M...>
{
    inline static std::vector<jfieldID> java_fields;
    inline static std::tuple<M...>      cpp_members{};

    inline static jclass    javaClass;
    inline static jmethodID javaInit;

    static void init(JNIEnv *env, const char* klass, FieldInfo<M>&&... info)
    {
        java_fields.clear();

        jclass klass_ = env->FindClass(klass);

        (java_fields.push_back(env->GetFieldID(klass_, info.name, info.sig)), ...);
        cpp_members = std::make_tuple(info.member_ptr...);

        javaClass = reinterpret_cast<jclass>(env->NewGlobalRef(klass_));
        env->DeleteLocalRef(klass_);

        javaInit = env->GetMethodID(javaClass, "<init>", "()V");
    }

    static void destruct(JNIEnv* env)
    {
        env->DeleteGlobalRef(javaClass);
    }
};

template<class CppType, std::enable_if_t<!std::disjunction_v<std::is_enum<CppType>, IsInputType<CppType>>, bool> = true>
void copy_from_java(JNIEnv* env, const jobject src, CppType& dest)
{
    auto getter = [&] (auto&&... member_ptr) {
        int i = -1;
        (get_field(env, src, Klass<CppType>::java_fields[++i], std::invoke(member_ptr, dest)), ...);
    };

    std::apply(getter, Klass<CppType>::cpp_members);
}

template<class CppType, std::enable_if_t<!std::disjunction_v<std::is_enum<CppType>, IsInputType<CppType>>, bool> = true>
void get_field(JNIEnv* env, const jobject obj, jfieldID id, CppType& dest)
{
    copy_from_java(env, env->GetObjectField(obj, id), dest);
}

template<class CppType, RequireNotEnum<CppType> = true>
CppType from_java(JNIEnv* env, const jobject src)
{
    CppType dest{};
    copy_from_java(env, src, dest);
    return dest;
}

template<class CppType, RequireNotEnum<CppType> = true>
void copy_to_java(const CppType& src, JNIEnv* env, jobject dest)
{
    auto setter = [&] (auto&&... member_ptr) {
        int i = -1;
        (set_field(std::invoke(member_ptr, src), env, dest, Klass<CppType>::java_fields[++i]), ...);
    };

    std::apply(setter, Klass<CppType>::cpp_members);
}

template<class CppType, RequireNotEnum<CppType> = true>
void set_field(const CppType& src, JNIEnv* env, jobject obj, jfieldID id)
{
    copy_to_java(src, env, env->GetObjectField(obj, id));
}

template<class CppType, RequireNotEnum<CppType> = true>
jobject to_java(const CppType& src, JNIEnv* env)
{
    jobject out = env->NewObject(Klass<CppType>::javaClass, Klass<CppType>::javaInit);
    copy_to_java(src, env, out);
    return out;
}


// ---------------------------------------------------------------------------------------------------------------------
// Enums
// ---------------------------------------------------------------------------------------------------------------------
template<class E, class... T>
struct KlassImpl<E, std::enable_if_t<std::is_enum_v<E>>, T...>
{
    inline static std::vector<jobject> java_members;
    inline static std::vector<E>       cpp_members;

    static void init(JNIEnv* env, const char* name, EnumInfo<T>... info)
    {
        std::string sig = std::string{"L"}.append(name).append(";");

        java_members.clear();
        cpp_members.clear();

        jclass klass = env->FindClass(name);

        (java_members.push_back(getMember(env, klass, info.name, sig.c_str())), ...);
        (cpp_members.push_back(info.value), ...);

        env->DeleteLocalRef(klass);
    }

    static void destruct(JNIEnv* env)
    {
        for (jobject member : java_members)    env->DeleteGlobalRef(member);
    }

private:
    static jobject getMember(JNIEnv* env, jclass klass, const char* name, const char* sig)
    {
        return env->NewGlobalRef(env->GetStaticObjectField(klass, env->GetStaticFieldID(klass, name, sig)));
    }
};

template<class CppType, RequireEnum<CppType> = true>
void get_field(JNIEnv* env, const jobject obj, jfieldID id, CppType& dest)
{
    dest = from_java<CppType>(env, env->GetObjectField(obj, id));
}

template<class CppType, RequireEnum<CppType> = true>
CppType from_java(JNIEnv* env, const jobject src)
{
    for (int i = 0; i < Klass<CppType>::java_members.size(); ++i)
    {
        if (env->IsSameObject(src, Klass<CppType>::java_members[i]))    return Klass<CppType>::cpp_members[i];
    }

    assert(((void)"Invalid enum value.", true));
}

template<class CppType, RequireEnum<CppType> = true>
void set_field(const CppType& src, JNIEnv* env, jobject obj, jfieldID id)
{
    env->SetObjectField(obj, id, to_java(src, env));
}

template<class CppType, RequireEnum<CppType> = true>
jobject to_java(const CppType& src, JNIEnv* env)
{
    for (int i = 0; i < Klass<CppType>::cpp_members.size(); ++i)
    {
        if (src == Klass<CppType>::cpp_members[i])    return Klass<CppType>::java_members[i];
    }

    assert(((void)"Invalid enum value.", true));
}


// ---------------------------------------------------------------------------------------------------------------------
// Params
// ---------------------------------------------------------------------------------------------------------------------
struct KlassDoubles
{
    inline static jclass    doublesClass;
    inline static jmethodID double_;

    static void init(JNIEnv* env)
    {
        doublesClass = reinterpret_cast<jclass>(env->NewGlobalRef(env->FindClass("java/lang/Double")));
        double_      = env->GetMethodID(doublesClass, "doubleValue","()D");
    }

    static void destruct(JNIEnv* env)
    {
        env->DeleteGlobalRef(doublesClass);
    }
};

template<class M, class CppType, RequireInput<CppType> = true>
void get_field_if_not_null(JNIEnv* env, M member_ptr, const jobject obj, int i, CppType& dest)
{
    jobject d = env->GetObjectField(obj, Klass<CppType>::java_fields[i]);

    if (d)    std::invoke(member_ptr,dest) = env->CallDoubleMethod(d,KlassDoubles::double_);
}

template<class CppType, RequireInput<CppType> = true>
void copy_from_java(JNIEnv* env, const jobject src, CppType& dest)
{
    auto getter = [&] (auto&&... member_ptr) {
        int i = -1;
        (get_field_if_not_null(env, member_ptr, src, ++i, dest),...);
    };

    std::apply(getter, Klass<CppType>::cpp_members);
}

template<class CppType, RequireInput<CppType> = true>
void get_field(JNIEnv* env, const jobject obj, jfieldID id, CppType& dest)
{
    jobject d = env->GetObjectField(obj, id);

    if (d)    copy_from_java(env, d, dest);
}


ENUM_CONVERTER(BodyState,
   ENUM_MEMBER(FED_RESTING),
   ENUM_MEMBER(FED_EXERCISING),
   ENUM_MEMBER(POSTABSORPTIVE_RESTING),
   ENUM_MEMBER(POSTABSORPTIVE_EXERCISING)
)

FIELD_CONVERTER(BloodParams,
    FIELD(rbcBirthRate,       "Ljava/lang/Double;"),
    FIELD(glycationProbSlope, "Ljava/lang/Double;"),
    FIELD(glycationProbConst, "Ljava/lang/Double;"),
    FIELD(glycolysisMin,      "Ljava/lang/Double;"),
    FIELD(glycolysisMax,      "Ljava/lang/Double;"),
    FIELD(minGlucoseLevel,    "Ljava/lang/Double;"),
    FIELD(baseGlucoseLevel,   "Ljava/lang/Double;"),
    FIELD(highGlucoseLevel,   "Ljava/lang/Double;"),
    FIELD(highLactateLevel,   "Ljava/lang/Double;"),
    FIELD(peakInsulinLevel,   "Ljava/lang/Double;"),
    FIELD(baseInsulinLevel,   "Ljava/lang/Double;")
)

FIELD_CONVERTER(BrainParams,
    FIELD(glucoseOxidized,  "Ljava/lang/Double;"),
    FIELD(glucoseToAlanine, "Ljava/lang/Double;"),
    FIELD(bAAToGlutamine,   "Ljava/lang/Double;")
)

FIELD_CONVERTER(HeartParams,
    FIELD(basalGlucoseAbsorbed, "Ljava/lang/Double;"),
    FIELD(Glut4Km,              "Ljava/lang/Double;"),
    FIELD(Glut4VMAX,            "Ljava/lang/Double;"),
    FIELD(lactateOxidized,      "Ljava/lang/Double;")
)

FIELD_CONVERTER(HumanParams,
    FIELD(age,                                          "Ljava/lang/Double;"),
    FIELD(gender,                                       "Ljava/lang/Double;"),
    FIELD(fitnessLevel,                                 "Ljava/lang/Double;"),
    FIELD(bodyWeight,                                   "Ljava/lang/Double;"),
    FIELD(glut4Impact,                                  "Ljava/lang/Double;"),
    FIELD(liverGlycogenSynthesisImpact,                 "Ljava/lang/Double;"),
    FIELD(glycolysisMinImpact,                          "Ljava/lang/Double;"),
    FIELD(glycolysisMaxImpact,                          "Ljava/lang/Double;"),
    FIELD(excretionKidneysImpact,                       "Ljava/lang/Double;"),
    FIELD(intensityPeakGlucoseProd,                     "Ljava/lang/Double;"),
    FIELD(gngImpact,                                    "Ljava/lang/Double;"),
    FIELD(liverGlycogenBreakdownImpact,                 "Ljava/lang/Double;"),
    FIELD(insulinImpactOnGlycolysis_Mean,               "Ljava/lang/Double;"),
    FIELD(insulinImpactOnGlycolysis_StdDev,             "Ljava/lang/Double;"),
    FIELD(insulinImpactOnGNG_Mean,                      "Ljava/lang/Double;"),
    FIELD(insulinImpactOnGNG_StdDev,                    "Ljava/lang/Double;"),
    FIELD(insulinImpactGlycogenBreakdownInLiver_Mean,   "Ljava/lang/Double;"),
    FIELD(insulinImpactGlycogenBreakdownInLiver_StdDev, "Ljava/lang/Double;"),
    FIELD(insulinImpactGlycogenSynthesisInLiver_Mean,   "Ljava/lang/Double;"),
    FIELD(insulinImpactGlycogenSynthesisInLiver_StdDev, "Ljava/lang/Double;")
)

FIELD_CONVERTER(IntestineParams,
    FIELD(aminoAcidsAbsorptionRate,   "Ljava/lang/Double;"),
    FIELD(glutamineOxidationRate,     "Ljava/lang/Double;"),
    FIELD(glutamineToAlanineFraction, "Ljava/lang/Double;"),
    FIELD(Glut2Km_In,                 "Ljava/lang/Double;"),
    FIELD(Glut2VMAX_In,               "Ljava/lang/Double;"),
    FIELD(Glut2VMAX_In,               "Ljava/lang/Double;"),
    FIELD(Glut2Km_Out,                "Ljava/lang/Double;"),
    FIELD(Glut2VMAX_Out,              "Ljava/lang/Double;"),
    FIELD(sglt1Rate,                  "Ljava/lang/Double;"),
    FIELD(fluidVolumeInEnterocytes,   "Ljava/lang/Double;"),
    FIELD(fluidVolumeInLumen,         "Ljava/lang/Double;"),
    FIELD(glycolysisMin,              "Ljava/lang/Double;"),
    FIELD(glycolysisMax,              "Ljava/lang/Double;"),
    FIELD(RAG_Mean,                   "Ljava/lang/Double;"),
    FIELD(RAG_StdDev,                 "Ljava/lang/Double;"),
    FIELD(SAG_Mean,                   "Ljava/lang/Double;"),
    FIELD(SAG_StdDev,                 "Ljava/lang/Double;")
)

FIELD_CONVERTER(KidneysParams,
    FIELD(glycolysisMin, "Ljava/lang/Double;"),
    FIELD(glycolysisMax, "Ljava/lang/Double;"),
    FIELD(gngKidneys,    "Ljava/lang/Double;")
)

FIELD_CONVERTER(LiverParams,
    FIELD(glycogen,                    "Ljava/lang/Double;"),
    FIELD(glycogenMax,                 "Ljava/lang/Double;"),
    FIELD(glycogenToGlucoseInLiver,    "Ljava/lang/Double;"),
    FIELD(glucoseToGlycogenInLiver,    "Ljava/lang/Double;"),
    FIELD(maxLipogenesis,              "Ljava/lang/Double;"),
    FIELD(glycolysisMin,               "Ljava/lang/Double;"),
    FIELD(glycolysisMax,               "Ljava/lang/Double;"),
    FIELD(glycolysisToLactateFraction, "Ljava/lang/Double;"),
    FIELD(gngLiver,                    "Ljava/lang/Double;"),
    FIELD(glucoseToNEFA,               "Ljava/lang/Double;"),
    FIELD(fluidVolume,                 "Ljava/lang/Double;"),
    FIELD(Glut2Km,                     "Ljava/lang/Double;"),
    FIELD(Glut2VMAX,                   "Ljava/lang/Double;")
)

FIELD_CONVERTER(MusclesParams,
    FIELD(glycogen,                            "Ljava/lang/Double;"),
    FIELD(glycogenMax,                         "Ljava/lang/Double;"),
    FIELD(glycogenShareDuringExerciseMean,     "Ljava/lang/Double;"),
    FIELD(basalGlucoseAbsorbed,                "Ljava/lang/Double;"),
    FIELD(maxGlucoseAbsorptionDuringExercise,  "Ljava/lang/Double;"),
    FIELD(baaToGlutamine,                      "Ljava/lang/Double;"),
    FIELD(glycolysisMin,                       "Ljava/lang/Double;"),
    FIELD(glycolysisMax,                       "Ljava/lang/Double;"),
    FIELD(glucoseToGlycogen,                   "Ljava/lang/Double;"),
    FIELD(Glut4Km,                             "Ljava/lang/Double;"),
    FIELD(Glut4VMAX,                           "Ljava/lang/Double;"),
    FIELD(peakGlut4VMAX,                       "Ljava/lang/Double;")
)

FIELD_CONVERTER(PortalVeinParams,
    FIELD(fluidVolume, "Ljava/lang/Double;")
)

FIELD_CONVERTER(StomachParams,
    FIELD(geConstant, "Ljava/lang/Double;"),
    FIELD(geSlopeMin, "Ljava/lang/Double;")
)

FIELD_CONVERTER(MetabolicParams,
    FIELD(blood,      "Ledu/uwm/cs/cs595/carbmetsimapp/BloodParams;"),
    FIELD(brain,      "Ledu/uwm/cs/cs595/carbmetsimapp/BrainParams;"),
    FIELD(heart,      "Ledu/uwm/cs/cs595/carbmetsimapp/HeartParams;"),
    FIELD(body,       "Ledu/uwm/cs/cs595/carbmetsimapp/HumanParams;"),
    FIELD(intestine,  "Ledu/uwm/cs/cs595/carbmetsimapp/IntestineParams;"),
    FIELD(kidneys,    "Ledu/uwm/cs/cs595/carbmetsimapp/KidneysParams;"),
    FIELD(liver,      "Ledu/uwm/cs/cs595/carbmetsimapp/LiverParams;"),
    FIELD(muscles,    "Ledu/uwm/cs/cs595/carbmetsimapp/MusclesParams;"),
    FIELD(portalVein, "Ledu/uwm/cs/cs595/carbmetsimapp/PortalVeinParams;"),
    FIELD(stomach,    "Ledu/uwm/cs/cs595/carbmetsimapp/StomachParams;")
)


// ---------------------------------------------------------------------------------------------------------------------
// Events
// ---------------------------------------------------------------------------------------------------------------------
ENUM_CONVERTER(EventType,
    ENUM_MEMBER(FOOD),
    ENUM_MEMBER(EXERCISE),
    ENUM_MEMBER(HALT)
)

FIELD_CONVERTER(ExerciseType,
    FIELD(exerciseID, "I"),
    FIELD(name,       "Ljava/lang/String;"),
    FIELD(intensity,  "D")
)

FIELD_CONVERTER(FoodType,
    FIELD(foodID,      "I"),
    FIELD(name,        "Ljava/lang/String;"),
    FIELD(servingSize, "D"),
    FIELD(RAG,         "D"),
    FIELD(SAG,         "D"),
    FIELD(protein,     "D"),
    FIELD(fat,         "D")
)

FIELD_CONVERTER(ExerciseEvent,
    FIELD(fireTime,   "I"),
    FIELD(eventType,  "Ledu/uwm/cs/cs595/carbmetsimapp/EventType;"),
    FIELD(duration,   "I"),
    FIELD(exerciseID, "I")
)

template<>
ExerciseEvent from_java(JNIEnv* env, const jobject src)
{
    ExerciseEvent dest{0, 0, 0};
    copy_from_java(env, src, dest);
    return dest;
}

jobject to_java(const std::shared_ptr<ExerciseEvent>& src, JNIEnv* env)
{
    return to_java(*src.get(), env);
}

FIELD_CONVERTER(FoodEvent,
    FIELD(fireTime,  "I"),
    FIELD(eventType, "Ledu/uwm/cs/cs595/carbmetsimapp/EventType;"),
    FIELD(quantity,  "I"),
    FIELD(foodID,    "I")
)

template<>
FoodEvent from_java(JNIEnv* env, const jobject src)
{
    FoodEvent dest{0, 0, 0};
    copy_from_java(env, src, dest);
    return dest;
}

jobject to_java(const std::shared_ptr<FoodEvent>& src, JNIEnv* env)
{
    return to_java(*src.get(), env);
}

FIELD_CONVERTER(HaltEvent,
    FIELD(fireTime,  "I"),
    FIELD(eventType, "Ledu/uwm/cs/cs595/carbmetsimapp/EventType;")
)

template<>
HaltEvent from_java(JNIEnv* env, const jobject src)
{
    HaltEvent dest{0};
    copy_from_java(env, src, dest);
    return dest;
}

jobject to_java(const std::shared_ptr<HaltEvent>& src, JNIEnv* env)
{
    return to_java(*src.get(), env);
}

struct KlassEvent
{
    inline static jclass   eventClass;
    inline static jfieldID eventType_;

    static void destruct(JNIEnv* env)
    {
        env->DeleteGlobalRef(eventClass);
    }
};

extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Event_00024Companion_onClassInit(JNIEnv* env, jobject thiz)
{
    KlassEvent::eventClass = reinterpret_cast<jclass>(env->NewGlobalRef(
        env->FindClass("edu/uwm/cs/cs595/carbmetsimapp/Event")));
    KlassEvent::eventType_ = env->GetFieldID(KlassEvent::eventClass, "eventType",
        "Ledu/uwm/cs/cs595/carbmetsimapp/EventType;");
}

jobject to_java(const std::shared_ptr<Event>& src, JNIEnv* env)
{
    switch (src->eventType)
    {
        case EventType::FOOD:        return to_java(std::dynamic_pointer_cast<FoodEvent>(src), env);
        case EventType::EXERCISE:    return to_java(std::dynamic_pointer_cast<ExerciseEvent>(src), env);
        case EventType::HALT:        return to_java(std::dynamic_pointer_cast<HaltEvent>(src), env);
    }
}

jobject to_java(const std::vector<std::shared_ptr<Event>>& src, JNIEnv* env)
{
    jobject out = env->NewObject(KlassArrayList::arrayListClass, KlassArrayList::arrayList_,
        static_cast<jsize>(src.size()));

    for (auto event: src)    env->CallBooleanMethod(out, KlassArrayList::add_, to_java(event, env));

    return out;
}

template<>
std::shared_ptr<Event> from_java(JNIEnv* env, const jobject src)
{
    EventType type;
    get_field<EventType>(env, src, KlassEvent::eventType_, type);

    switch (type)
    {
        case EventType::FOOD:        return std::make_shared<FoodEvent>(from_java<FoodEvent>(env, src));
        case EventType::EXERCISE:    return std::make_shared<ExerciseEvent>(from_java<ExerciseEvent>(env, src));
        case EventType::HALT:        return std::make_shared<HaltEvent>(from_java<HaltEvent>(env, src));
    }
}


// ---------------------------------------------------------------------------------------------------------------------
// Organs
// ---------------------------------------------------------------------------------------------------------------------
FIELD_CONVERTER(FatState,
    FIELD(amount,     "D"),
    FIELD(bodyWeight, "D")
)

FIELD_CONVERTER(GlucoseState,
    FIELD(amount,      "D"),
    FIELD(bloodBGL,    "D"),
    FIELD(bloodMinBGL, "D")
)

FIELD_CONVERTER(TotalsState,
    FIELD(glycolysis,               "D"),
    FIELD(excretion,                "D"),
    FIELD(oxidation,                "D"),
    FIELD(GNG,                      "D"),
    FIELD(liverGlycogenStorage,     "D"),
    FIELD(liverGlycogenBreakdown,   "D"),
    FIELD(musclesGlycogenStorage,   "D"),
    FIELD(musclesGlycogenBreakdown, "D"),
    FIELD(glucoseFromIntestine,     "D")
)

FIELD_CONVERTER(ChymeConsumed,
    FIELD(RAG,         "D"),
    FIELD(SAG,         "D"),
    FIELD(origRAG,     "D"),
    FIELD(origSAG,     "D"),
    FIELD(RAGConsumed, "D"),
    FIELD(SAGConsumed, "D")
)

template<class T>
void set_field(const std::vector<T>& src, JNIEnv* env, jobject obj, jfieldID id)
{
    jobject out = env->NewObject(KlassArrayList::arrayListClass, KlassArrayList::arrayList_,
                                 static_cast<jsize>(src.size()));

    for (auto t: src)    env->CallBooleanMethod(out, KlassArrayList::add_, to_java(t, env));

    env->SetObjectField(obj, id, out);
    env->DeleteLocalRef(out);
}

FIELD_CONVERTER(GlycogenSynthesisState,
    FIELD(glycogen, "D"),
    FIELD(glucose,  "D")
)

FIELD_CONVERTER(GluconeogenesisState,
    FIELD(glucose,      "D"),
    FIELD(glycogen,     "D"),
    FIELD(bloodGlucose, "D"),
    FIELD(bloodLactate, "D")
)

FIELD_CONVERTER(Blood,
    FIELD(getBGL,               "D"),
    FIELD(baseBGL,              "D"),
    FIELD(volume,               "D"),
    FIELD(minGlucoseLevel,      "D"),
    FIELD(glucose,              "D"),
    FIELD(baseInsulinLevel,     "D"),
    FIELD(insulinLevel,         "D"),
    FIELD(lactate,              "D"),
    FIELD(branchedAminoAcids,   "D"),
    FIELD(glutamine,            "D"),
    FIELD(alanine,              "D"),
    FIELD(unbranchedAminoAcids, "D"),
    FIELD(gngSubstrates,        "D"),
    FIELD(glycolysisPerTick,    "D"),
    FIELD(totalGlycolysisSoFar, "D"),
    ARRAYFIELD(killRate,        "[D", (Blood::MAX_AGE - Blood::HUNDRED_DAYS)),
    ARRAYFIELD(killBin,         "[I", (Blood::MAX_AGE - Blood::HUNDRED_DAYS)),
    FIELD(RBCsUpdated,          "Z"),
    FIELD(glLumen,              "D"),
    FIELD(glEnterocytes,        "D"),
    FIELD(glPortalVein,         "D")
)

FIELD_CONVERTER(Brain,
    FIELD(oxidationPerTick, "D"),
    FIELD(glucoseRemoved,   "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;")
)

FIELD_CONVERTER(Heart,
    FIELD(oxidationPerTick, "D"),
    FIELD(basalAbsorption,  "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;")
)

FIELD_CONVERTER(HumanBody,
    FIELD(bodyState,                    "Ledu/uwm/cs/cs595/carbmetsimapp/BodyState;"),
    FIELD(glut4Impact,                  "D"),
    FIELD(liverGlycogenSynthesisImpact, "D"),
    FIELD(excretionKidneysImpact,       "D"),
    FIELD(age,                          "I"),
    FIELD(gender,                       "I"),
    FIELD(fitnessLevel,                 "I"),
    FIELD(VO2Max,                       "D"),
    FIELD(percentVO2Max,                "D"),
    FIELD(bodyWeight,                   "D"),
    FIELD(fatFraction,                  "D"),
    FIELD(exerciseOverAt,               "I"),
    FIELD(lastHardExerciseAt,           "I"),
    FIELD(isExercising,                 "Z"),
    FIELD(totalGlycolysisSoFar,                "D"),
    FIELD(totalGNGSoFar,                       "D"),
    FIELD(totalOxidationSoFar,                 "D"),
    FIELD(totalLiverGlycogenStorageSoFar,      "D"),
    FIELD(totalLiverGlycogenBreakdownSoFar,    "D"),
    FIELD(totalMusclesGlycogenStorageSoFar,    "D"),
    FIELD(totalMusclesGlycogenBreakdownSoFar,  "D"),
    FIELD(totalEndogeneousGlucoseReleaseSoFar, "D"),
    FIELD(totalGlucoseReleaseSoFar,            "D"),
    FIELD(getTotals,                           "Ledu/uwm/cs/cs595/carbmetsimapp/TotalsState;"),
    FIELD(tempGNG,                                    "D"),
    FIELD(tempGlycolysis,                             "D"),
    FIELD(tempOxidation,                              "D"),
    FIELD(tempExcretion,                              "D"),
    FIELD(tempGlycogenStorage,                        "D"),
    FIELD(tempGlycogenBreakdown,                      "D"),
    FIELD(baseBGL,                                    "D"),
    FIELD(peakBGL,                                    "D"),
    FIELD(intensityPeakGlucoseProd,                   "D"),
    FIELD(insulinImpactOnGNG_Mean,                    "D"),
    FIELD(insulinImpactGlycogenBreakdownInLiver_Mean, "D"),
    FIELD(dayEndTotals,                          "Ledu/uwm/cs/cs595/carbmetsimapp/TotalsState;"),
    FIELD(totalGlycolysisPerTick,                "D"),
    FIELD(totalGNGPerTick,                       "D"),
    FIELD(totalOxidationPerTick,                 "D"),
    FIELD(totalGlycogenStoragePerTick,           "D"),
    FIELD(totalGlycogenBreakdownPerTick,         "D"),
    FIELD(totalEndogeneousGlucoseReleasePerTick, "D"),
    FIELD(totalGlucoseReleasePerTick,            "D")
)

FIELD_CONVERTER(Intestine,
    FIELD(glycolysisPerTick,         "D"),
    FIELD(toPortalVeinPerTick,       "D"),
    FIELD(chymeConsumed,             "Ljava/util/ArrayList;"),
    FIELD(totalRAGConsumed,          "D"),
    FIELD(totalSAGConsumed,          "D"),
    FIELD(activeAbsorption,          "D"),
    FIELD(passiveAbsorption,         "D"),
    FIELD(glLumen,                   "D"),
    FIELD(glEnterocytes,             "D"),
    FIELD(glPortalVein,              "D"),
    FIELD(glPortalVeinConcentration, "D"),
    FIELD(glucoseFromBlood,          "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;")
)

FIELD_CONVERTER(Kidneys,
    FIELD(totalExcretion,        "D"),
    FIELD(glycolysisPerTick,     "D"),
    FIELD(gngPerTick,            "D"),
    FIELD(excretionPerTick,      "D"),
    FIELD(toGlycolysis,          "D"),
    FIELD(postGlycolysis,        "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;"),
    FIELD(postGluconeogenesis,   "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;"),
    FIELD(postGlucoseExtraction, "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;")
)

FIELD_CONVERTER(Liver,
    FIELD(glycogen,                "D"),
    FIELD(fluidVolume,             "D"),
    FIELD(glucose,                 "D"),
    FIELD(absorptionPerTick,       "D"),
    FIELD(toGlycogenPerTick,       "D"),
    FIELD(fromGlycogenPerTick,     "D"),
    FIELD(glycolysisPerTick,       "D"),
    FIELD(gngPerTick,              "D"),
    FIELD(releasePerTick,          "D"),
    FIELD(excessGlucoseAbsorption, "D"),
    FIELD(glucoseNeeded,           "D"),
    FIELD(maxGNGDuringExercise,    "D"),
    FIELD(lipogenesisOccurred,     "Z"),
    FIELD(postGlycogen,            "Ledu/uwm/cs/cs595/carbmetsimapp/GlycogenSynthesisState;"),
    FIELD(postGluconeogenesis,     "Ledu/uwm/cs/cs595/carbmetsimapp/GluconeogenesisState;")
)

FIELD_CONVERTER(Muscles,
    FIELD(glucoseAbsorbedPerTick,     "D"),
    FIELD(glycogenSynthesizedPerTick, "D"),
    FIELD(glycogenBreakdownPerTick,   "D"),
    FIELD(oxidationPerTick,           "D"),
    FIELD(glycogenOxidizedPerTick,    "D"),
    FIELD(glycolysisPerTick,          "D"),
    FIELD(hadExercise,                "Z"),
    FIELD(absorbedFromBlood,          "D"),
    FIELD(absorptionState,            "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;"),
    FIELD(glycogenShare,              "D"),
    FIELD(basalBase,                  "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;"),
    FIELD(basalGLUT4,                 "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;"),
    FIELD(basalGLUT4Occurred,         "Z"),
    FIELD(glycogen,                   "D")
)

FIELD_CONVERTER(PortalVein,
    FIELD(getConcentration, "D"),
    FIELD(fromBlood,        "Ledu/uwm/cs/cs595/carbmetsimapp/GlucoseState;")
)

FIELD_CONVERTER(Stomach,
    FIELD(RAG,                "D"),
    FIELD(SAG,                "D"),
    FIELD(protein,            "D"),
    FIELD(fat,                "D"),
    FIELD(totalFood,          "D"),
    FIELD(calorificDensity,   "D"),
    FIELD(geSlope,            "D"),
    FIELD(ragInBolus,         "D"),
    FIELD(sagInBolus,         "D"),
    FIELD(proteinInBolus,     "D"),
    FIELD(fatInBolus,         "Ledu/uwm/cs/cs595/carbmetsimapp/FatState;"),
    FIELD(stomachEmpty,       "Z"),
    FIELD(stomachBecameEmpty, "Z")
)

#undef FIELD_CONVERTER
#undef ARRAYFIELD
#undef FIELD
#undef ENUM_CONVERTER
#undef ENUM_MEMBER
