#include <jni.h>
#include "carbmetlib/source/SimCtl.h"
#include "converters.h"
#include "KlassHashMap.h"


// ---------------------------------------------------------------------------------------------------------------------
// Native interface
// ---------------------------------------------------------------------------------------------------------------------
// Use to call native code from Kotlin.

// JNI specifications: https://docs.oracle.com/en/java/javase/15/docs/specs/jni/index.html
// JNI function reference: https://docs.oracle.com/en/java/javase/15/docs/specs/jni/functions.html

// Recommend to use context actions to create a stub JNI function.
// For manual creation, see https://docs.oracle.com/en/java/javase/15/docs/specs/jni/types.html
// and https://docs.oracle.com/en/java/javase/15/docs/specs/jni/design.html
// for function signature mangling rules (when overloading functions).

namespace KlassSimulator
{
    inline static jfieldID nativeHandle_;
    inline static jfieldID seedString_;
    inline static jfieldID metabolicParameters_;
    inline static jfieldID foodTypes_;
    inline static jfieldID exerciseTypes_;
    inline static jfieldID blood_;
    inline static jfieldID body_;
    inline static jfieldID brain_;
    inline static jfieldID heart_;
    inline static jfieldID intestine_;
    inline static jfieldID kidneys_;
    inline static jfieldID liver_;
    inline static jfieldID muscles_;
    inline static jfieldID portalVein_;
    inline static jfieldID stomach_;

    SimCtl* native(JNIEnv* env, jobject thiz)
    {
        return reinterpret_cast<SimCtl*>(env->GetLongField(thiz, nativeHandle_));
    }

    std::string seedString(JNIEnv* env, jobject thiz)
    {
        return from_java<std::string>(env, env->GetObjectField(thiz, seedString_));
    }

    std::map<BodyState, MetabolicParams> metabolicParams(JNIEnv* env, jobject thiz)
    {
        return from_java<std::map<BodyState, MetabolicParams>>(env, env->GetObjectField(thiz, metabolicParameters_));
    }

    std::map<unsigned, FoodType> foodTypes(JNIEnv* env, jobject thiz)
    {
        return from_java<std::map<unsigned, FoodType>>(env, env->GetObjectField(thiz, foodTypes_));
    }

    std::map<unsigned, ExerciseType> exerciseTypes(JNIEnv* env, jobject thiz)
    {
        return from_java<std::map<unsigned, ExerciseType>>(env, env->GetObjectField(thiz, exerciseTypes_));
    }

    void init(JNIEnv* env)
    {
        jclass simulatorClass = env->FindClass("edu/uwm/cs/cs595/carbmetsimapp/Simulator");

        nativeHandle_        = env->GetFieldID(simulatorClass, "nativeHandle",        "J");
        seedString_          = env->GetFieldID(simulatorClass, "seedString",          "Ljava/lang/String;");
        metabolicParameters_ = env->GetFieldID(simulatorClass, "metabolicParameters", "Ljava/util/HashMap;");
        foodTypes_           = env->GetFieldID(simulatorClass, "foodTypes",           "Ljava/util/HashMap;");
        exerciseTypes_       = env->GetFieldID(simulatorClass, "exerciseTypes",       "Ljava/util/HashMap;");
        blood_      = env->GetFieldID(simulatorClass, "blood",      "Ledu/uwm/cs/cs595/carbmetsimapp/Blood;");
        body_       = env->GetFieldID(simulatorClass, "body",       "Ledu/uwm/cs/cs595/carbmetsimapp/HumanBody;");
        brain_      = env->GetFieldID(simulatorClass, "brain",      "Ledu/uwm/cs/cs595/carbmetsimapp/Brain;");
        heart_      = env->GetFieldID(simulatorClass, "heart",      "Ledu/uwm/cs/cs595/carbmetsimapp/Heart;");
        intestine_  = env->GetFieldID(simulatorClass, "intestine",  "Ledu/uwm/cs/cs595/carbmetsimapp/Intestine;");
        kidneys_    = env->GetFieldID(simulatorClass, "kidneys",    "Ledu/uwm/cs/cs595/carbmetsimapp/Kidneys;");
        liver_      = env->GetFieldID(simulatorClass, "liver",      "Ledu/uwm/cs/cs595/carbmetsimapp/Liver;");
        muscles_    = env->GetFieldID(simulatorClass, "muscles",    "Ledu/uwm/cs/cs595/carbmetsimapp/Muscles;");
        portalVein_ = env->GetFieldID(simulatorClass, "portalVein", "Ledu/uwm/cs/cs595/carbmetsimapp/PortalVein;");
        stomach_    = env->GetFieldID(simulatorClass, "stomach",    "Ledu/uwm/cs/cs595/carbmetsimapp/Stomach;");

        env->DeleteLocalRef(simulatorClass);
    }

    void update(JNIEnv* env, jobject thiz)
    {
        SimCtl* sim = native(env, thiz);

        set_field(*sim->blood,      env, thiz, blood_);
        set_field(*sim->body,       env, thiz, body_);
        set_field(*sim->brain,      env, thiz, brain_);
        set_field(*sim->heart,      env, thiz, heart_);
        set_field(*sim->intestine,  env, thiz, intestine_);
        set_field(*sim->kidneys,    env, thiz, kidneys_);
        set_field(*sim->liver,      env, thiz, liver_);
        set_field(*sim->muscles,    env, thiz, muscles_);
        set_field(*sim->portalVein, env, thiz, portalVein_);
        set_field(*sim->stomach,    env, thiz, stomach_);
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_addEvent__ILedu_uwm_cs_cs595_carbmetsimapp_EventType_2II(
    JNIEnv* env,
    jobject thiz,
    jint    fire_time,
    jobject type,
    jint    id,
    jint    howmuch
) {
    auto eventType_ = from_java<EventType>(env, type);
    KlassSimulator::native(env, thiz)->addEvent(fire_time, eventType_, id, howmuch);
}


extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_addEvent__Ledu_uwm_cs_cs595_carbmetsimapp_Event_2(
    JNIEnv* env,
    jobject thiz,
    jobject event
) {
    auto new_event = from_java<std::shared_ptr<Event>>(env, event);
    KlassSimulator::native(env, thiz)->addEvent(new_event);
}


extern "C"
JNIEXPORT jboolean JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_runTick(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->runTick();
}


extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_runToHalt(JNIEnv* env, jobject thiz)
{
    KlassSimulator::native(env, thiz)->runToHalt();
}


extern "C"
JNIEXPORT jboolean JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_eventsWereFired(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->eventsWereFired();
}


extern "C"
JNIEXPORT jobject JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_getFiredEvents(JNIEnv *env, jobject thiz)
{
    return to_java(KlassSimulator::native(env, thiz)->getFiredEvents(), env);
}


extern "C"
JNIEXPORT jint JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_elapsedDays(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->elapsedDays();
}


extern "C"
JNIEXPORT jint JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_elapsedHours(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->elapsedHours();
}


extern "C"
JNIEXPORT jint JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_elapsedMinutes(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->elapsedMinutes();
}


extern "C"
JNIEXPORT jint JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_ticks(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->ticks();
}


extern "C"
JNIEXPORT jboolean JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_dayOver(JNIEnv *env, jobject thiz)
{
    return KlassSimulator::native(env, thiz)->dayOver();
}


extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_update(JNIEnv* env, jobject thiz)
{
    KlassSimulator::update(env, thiz);
}


extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_nativeDestruct(JNIEnv* env, jobject thiz)
{
    delete KlassSimulator::native(env, thiz);
}


extern "C"
JNIEXPORT jint JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_00024Companion_timeToTicks(
    JNIEnv *env,
    jobject thiz,
    jint days,
    jint hours,
    jint minutes
) {
    return SimCtl::timeToTicks(days, hours, minutes);
}


extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_nativeInit(JNIEnv* env, jobject thiz)
{
    using namespace KlassSimulator;

    jlong handle = reinterpret_cast<jlong>(
        new SimCtl{seedString(env, thiz), metabolicParams(env, thiz), foodTypes(env, thiz), exerciseTypes(env, thiz)}
    );

    env->SetLongField(thiz, nativeHandle_, handle);
}


extern "C"
JNIEXPORT void JNICALL
Java_edu_uwm_cs_cs595_carbmetsimapp_Simulator_00024Companion_onClassInit(JNIEnv* env, jobject thiz)
{
    KlassSimulator::init(env);
    KlassArrayList::init(env);
    KlassHashMap::init(env);
    KlassDoubles::init(env);

    // Some classes seem not to be automatically loaded. Maybe because no instance is created by java until runtime?
    Java_edu_uwm_cs_cs595_carbmetsimapp_ChymeConsumed_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_BloodParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_BrainParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_HeartParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_HumanParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_IntestineParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_LiverParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_KidneysParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_MusclesParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_PortalVeinParams_00024Companion_onClassInit(env, thiz);
    Java_edu_uwm_cs_cs595_carbmetsimapp_StomachParams_00024Companion_onClassInit(env, thiz);
}


JNIEXPORT void JNI_OnUnload(JavaVM* vm, void* reserved)
{
    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)    return;

    // This might not be necessary, but it is good to clean up resources anyway.
    KlassArrayList::destruct(env);
    Klass<BodyState>::destruct(env);
    Klass<EventType>::destruct(env);
    Klass<ExerciseType>::destruct(env);
    Klass<FoodType>::destruct(env);
    Klass<ExerciseEvent>::destruct(env);
    Klass<FoodEvent>::destruct(env);
    Klass<HaltEvent>::destruct(env);
    KlassEvent::destruct(env);
    Klass<FatState>::destruct(env);
    Klass<GlucoseState>::destruct(env);
    Klass<ChymeConsumed>::destruct(env);
    Klass<GlycogenSynthesisState>::destruct(env);
    Klass<GluconeogenesisState>::destruct(env);
    Klass<Blood>::destruct(env);
    Klass<Brain>::destruct(env);
    Klass<Heart>::destruct(env);
    Klass<HumanBody>::destruct(env);
    Klass<Intestine>::destruct(env);
    Klass<Kidneys>::destruct(env);
    Klass<Liver>::destruct(env);
    Klass<Muscles>::destruct(env);
    Klass<PortalVein>::destruct(env);
    Klass<Stomach>::destruct(env);
    KlassDoubles::destruct(env);
}