package edu.uwm.cs.cs595.carbmetsimapp.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppBottomNavigation
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppNavigation


@Composable
fun DevMenu(goTo: AppNavigation) {
    Scaffold(
        topBar    = { TopAppBar(title = { Text(stringResource(R.string.devMenu)) }) },
        bottomBar = { AppBottomNavigation(goTo) }
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            TextButton(onClick = goTo.timeline)     { Text(stringResource(R.string.timeline)) }
            TextButton(onClick = goTo.addEvent)     { Text(stringResource(R.string.addEvent)) }
            TextButton(onClick = goTo.editActivity) { Text(stringResource(R.string.editActivity)) }
            TextButton(onClick = goTo.settings)     { Text(stringResource(R.string.settings)) }
            TextButton(onClick = goTo.stats)        { Text(stringResource(R.string.stats)) }
        }
    }
}

@Preview
@Composable
fun DevMenuPreview() {
    val navController = rememberNavController()
    val goTo = AppNavigation(navController)
    DevMenu(goTo)
}