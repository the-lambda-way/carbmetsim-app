package edu.uwm.cs.cs595.carbmetsimapp.ui

import android.content.Context
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.PopupProperties
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.qualifiers.ApplicationContext
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppBottomNavigation
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppNavigation
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppTopAppBar
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.validateNumber
import androidx.compose.ui.semantics.SemanticsProperties.ToggleableState
import java.lang.NumberFormatException

@Preview
@Composable
fun Settings(goTo: AppNavigation) {
    val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(LocalContext.current)
    val contextRef = LocalContext.current

    val userName = remember {  mutableStateOf(sharedPrefs.getString("name", "").toString())  }
    val userAge = remember { mutableStateOf(sharedPrefs.getInt("age", 0).toString()) }
    val activityLevel = remember { mutableStateOf(sharedPrefs.getFloat("activity_level", 0f).toString()) }
    val weight = remember { mutableStateOf(sharedPrefs.getFloat("weight", 0f).toString()) }
    val gender = remember { mutableStateOf(GENDER_SWITCH.valueOf(sharedPrefs.getString("gender", "Unset").toString())) }
    val diabetic = remember { mutableStateOf(sharedPrefs.getBoolean("diabetic", true)) }

    var errorMsg = remember { mutableStateOf("") }


    Scaffold(
        topBar    = { AppTopAppBar(title = R.string.settings, onBackButton = goTo.lastScreen) },
        bottomBar = { AppBottomNavigation(goTo) }
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            OutlinedTextField(
                value = userName.value,
                onValueChange = { userName.value = it },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                maxLines = 1,
                label = { Text("Name") }
            )

            OutlinedTextField(
                value = userAge.value.toString(),
                onValueChange = { userAge.value = it },
                modifier = Modifier,
                singleLine = true,
                maxLines = 1,
                label = { Text("Age") }
            )

            OutlinedTextField(
                value = activityLevel.value.toString(),
                onValueChange = { activityLevel.value = it },
                singleLine = true,
                maxLines = 1,
                label = { Text("Activity Level") }
            )

            OutlinedTextField(
                value = weight.value.toString(),
                onValueChange = { weight.value = it },
                singleLine = true,
                maxLines = 1,
                label = { Text("Weight (kg)") }
            )

            Row() {
                Checkbox(
                    checked = diabetic.value,
                    modifier = Modifier.padding(0.dp, 16.dp, 8.dp, 16.dp),
                    onCheckedChange = { diabetic.value = it }
                )
                Text("Diabetic", color = Color.DarkGray, modifier = Modifier.padding(0.dp, 16.dp, 16.dp, 16.dp))
            }

            Text("Gender:", color = Color.DarkGray, modifier = Modifier.padding(0.dp))
            Row(
                modifier = Modifier
                    .padding(8.dp)
            ){
                @Composable
                fun decideBtnColor (type : GENDER_SWITCH) : Color {
                    return (if (type == gender.component1()) colorResource(id = R.color.selected_button_color) else colorResource(
                        id = R.color.purple_500)
                            )
                }

                for( eachGender in GENDER_SWITCH.values() ) {
                    GenderChip(
                        genType = eachGender,
                        selColor = decideBtnColor(type = eachGender),
                        onChangeFun = { gender.value = eachGender }
                    )
                }
            }

            Button(
                onClick = {
                    saveSettings(
                        context = contextRef,
                        userName = userName.value,
                        userAge = userAge.value,
                        activityLevel = activityLevel.value,
                        weight = weight.value,
                        gender = gender.value.toString(),
                        diabetic = diabetic.value,
                        errorCallback = { errStr -> errorMsg.value = errStr }
                    )
                },
                Modifier
                    .fillMaxWidth()
                    .padding(start = 4.dp, top = 16.dp, bottom = 8.dp, end = 8.dp)
            ){
                Text("Save")
            }
        }
    }

    if (! errorMsg.component1().isEmpty()) {
        saveFailedToast(text = errorMsg.value)
        errorMsg.value = ""
    }
}

fun saveSettings(context : android.content.Context, userName : String, userAge : String, activityLevel : String, weight : String, gender : String, diabetic : Boolean,errorCallback : (String) -> Unit ) {
    // code for saving to SharedPreferences
    val prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    var success = true;

    try {
        prefEditor.putString("name", userName)
        prefEditor.putInt("age", userAge.toInt())
        prefEditor.putFloat("activity_level", activityLevel.toFloat())
        prefEditor.putFloat("weight", weight.toFloat())
        prefEditor.putString("gender", gender.toString())
        prefEditor.putBoolean("diabetic", diabetic)
    }
    catch (numEx: NumberFormatException) {
        errorCallback("Invalid number in one of the fields.")
        success = false;
    }
    catch (ex: Exception) {
        errorCallback("Something went wrong")
        success = false;
    }

    if ( success ) {
        prefEditor.commit()
        prefEditor.apply()
        errorCallback("Saved")
    }
}

@Composable
fun saveFailedToast(text: String) {
    Toast.makeText(LocalContext.current, text, Toast.LENGTH_LONG).show()
}

// same code as CategoryChip
@Composable
fun GenderChip(genType: GENDER_SWITCH, selColor: Color, isSelected: Boolean = false, onChangeFun: () -> Unit){
    Surface (
        modifier = Modifier.padding(end = 8.dp),
        elevation = 8.dp,
        shape = MaterialTheme.shapes.medium,
        color = MaterialTheme.colors.primary
    ){
        Column(
            modifier = Modifier.padding(1.dp)
        ) {
            Row(
                modifier = Modifier
                    .toggleable(
                        value = isSelected,
                        onValueChange = {}
                    )
            ) {
                Button(
                    /*style = MaterialTheme.typography.body2,*/
                    /*color = Color.White,*/
                    colors = ButtonDefaults.buttonColors(backgroundColor = selColor),
                    onClick = onChangeFun
                ) { Text(genType.toString()) }
            }

        }
    }
}

enum class GENDER_SWITCH( val genderSwitch : String ) {
    Unset("Unset"),
    Male("Male"),
    Female("Female")
    // other genders can be added when we understand how they tie into carbmetsim
}

@Preview
@Composable
fun SettingsPreview() {
    val navController = rememberNavController()
    val goTo = AppNavigation(navController)
    Settings(goTo)
}