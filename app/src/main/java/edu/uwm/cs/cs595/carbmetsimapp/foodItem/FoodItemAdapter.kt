package edu.uwm.cs.cs595.carbmetsimapp.foodItem

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.uwm.cs.cs595.carbmetsimapp.databinding.EventDesignBinding

class FoodItemAdapter: RecyclerView.Adapter<FoodItemAdapter.MyViewHolder>(){

    var foodItemList = emptyList<FoodItem>()

    class MyViewHolder(val binding: EventDesignBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val viewHolder = MyViewHolder(
            EventDesignBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.binding.addFood.setOnClickListener{
            if (viewHolder.binding.addFood.isChecked){
                val name = viewHolder.binding.foodEvent.text.toString()
                val food: FoodItem? = foodItemList.find {it.name == name}
                if (food != null) {
                    food.checked = true
                }
            }
            else{
                val name = viewHolder.binding.foodEvent.text.toString()
                val food: FoodItem? = foodItemList.find {it.name == name}
                if (food != null) {
                    food.checked = false
                }
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.foodEvent.text = foodItemList[position].name
    }

    override fun getItemCount(): Int {
        return foodItemList.size
    }

    fun setData(foodItem: List<FoodItem>){
        foodItemList = foodItem
        notifyDataSetChanged()
    }
}