package edu.uwm.cs.cs595.carbmetsimapp.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import edu.uwm.cs.cs595.carbmetsimapp.model.BeaglStatsConnector
import edu.uwm.cs.cs595.carbmetsimapp.model.DummyBeaglStats
import androidx.compose.ui.unit.sp
import java.util.*
import edu.uwm.cs.cs595.carbmetsimapp.model.StatsData
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.*
import java.time.LocalDate
import java.time.LocalTime

@Preview
@Composable
fun Stats(goTo: AppNavigation, statsProvider: BeaglStatsConnector) {
    Scaffold(
        topBar    = { AppTopAppBar(title = R.string.stats, onBackButton = goTo.lastScreen) },
        bottomBar = { AppBottomNavigation(goTo) }
    ) {
        Column(
            modifier = Modifier.background(colorResource(id = R.color.purple_200))
        ) {
            var selectedCat = remember { mutableStateOf(StatType.ALL_TIME) }
            var selectedDate = remember { mutableStateOf(LocalDate.now()) }

            fun getStats( type : StatType, inDate : LocalDate ) : StatsData {
                return when ( type ) {
                    StatType.ALL_TIME -> statsProvider.getAllTimeStats()
                    StatType.MONTHLY -> statsProvider.getMonthlyStats( inDate )
                    StatType.WEEKLY -> statsProvider.getWeeklyStats( inDate )
                    StatType.DAILY -> statsProvider.getDailyStats( inDate )
                }
            }

            var stats : MutableState<StatsData> = remember { mutableStateOf( getStats(selectedCat.component1(), selectedDate.component1()) ) }

            fun onChangeCat(newType : StatType) { selectedCat.value = newType;
                                                  stats.value = getStats(selectedCat.component1(),
                                                                         selectedDate.component1()) }

            Column(Modifier.border(border = BorderStroke(1.dp, color = colorResource(id = R.color.purple_700))) ) {
                Box(Modifier.clickable { onChangeCat(StatType.ALL_TIME) }) {
                    Text(
                        text = "All Time",
                        fontWeight = FontWeight.Light,
                        color = Color.White,
                        fontSize = 30.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                    )
                }

                // data
                if (selectedCat.component1() == StatType.ALL_TIME) {
                    showStats(data = stats.component1(),
                              date = selectedDate.component1(),
                              onCalChange = { newDate -> selectedDate.value = newDate },
                              type = StatType.ALL_TIME)
                }
            } // inner category column

            Column(Modifier.border(border = BorderStroke(1.dp, color = colorResource(id = R.color.purple_700)))) {
                Box(Modifier.clickable { onChangeCat(StatType.MONTHLY) }) {
                    Text(
                        "Monthly", fontWeight = FontWeight.Light,
                        color = Color.White,
                        fontSize = 30.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                    )
                }
                // data
                if (selectedCat.component1() == StatType.MONTHLY) {
                    showStats(data = stats.component1(),
                        date = selectedDate.component1(),
                        onCalChange = {
                                newDate -> selectedDate.value = newDate;
                                stats.value = getStats(selectedCat.component1(),
                                              selectedDate.component1()) }, StatType.MONTHLY)
                }
            } // inner category column

            Column(Modifier.border(border = BorderStroke(1.dp, color = colorResource(id = R.color.purple_700)))) {
                Box(Modifier.clickable { onChangeCat(StatType.WEEKLY) }) {
                    Text(
                        "Weekly", fontWeight = FontWeight.Light,
                        color = Color.White,
                        fontSize = 30.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                    )
                }
                // data
                if (selectedCat.component1() == StatType.WEEKLY) {
                    showStats(data = stats.component1(),
                              date = selectedDate.component1(),
                              onCalChange = {
                                      newDate -> selectedDate.value = newDate;
                                      stats.value = getStats(selectedCat.component1(),
                                      selectedDate.component1()) },
                              type = StatType.MONTHLY)
                }
            } // inner category column


                Column(
                    Modifier.border(
                        border = BorderStroke(
                            1.dp,
                            color = colorResource(id = R.color.purple_700)
                        )
                    )
                ) {
                    Box(Modifier.clickable { onChangeCat(StatType.DAILY) }) {
                        Text(
                            "Daily", fontWeight = FontWeight.Light,
                            color = Color.White,
                            fontSize = 30.sp,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp)
                        )
                    }
                    // data
                    if (selectedCat.component1() == StatType.DAILY) {
                        showStats(
                            data = stats.component1(),
                            date = selectedDate.component1(),
                            onCalChange = { newDate ->
                                selectedDate.value = newDate;
                                stats.value = getStats(
                                    selectedCat.component1(),
                                    selectedDate.component1()
                                )
                            },
                            StatType.DAILY
                        )
                    }
                } // inner category column

        }
    }
}

@Preview
@Composable
fun showStats(data : StatsData, date : LocalDate, onCalChange : (LocalDate) -> Unit, type : StatType ) {
    Column(
        Modifier
            .background(Color.White)
            .fillMaxWidth()) {
        if (type != StatType.ALL_TIME) {
            Row() {
                Text("Pick date", Modifier.padding(10.dp, 10.dp, 0.dp, 0.dp), color = colorResource(id = R.color.purple_500), fontWeight = FontWeight.Medium)
                DatePickerDialogBox(
                    onComplete = onCalChange
                ) { openDialog ->
                    DateButton(date, onClick = openDialog)
                }
            }
        }

        Text("Highest BGL: ${data.max} mg/dL", color = Color.DarkGray, modifier = Modifier.padding(10.dp))
        Text("Lowest BGL:  ${data.min} mg/dL", color = Color.DarkGray, modifier = Modifier.padding(10.dp))
        Text("Average BGL: ${data.average} mg/dL", color = Color.DarkGray, modifier = Modifier.padding(10.dp))
    }
}

enum class StatType {
    ALL_TIME,
    MONTHLY,
    WEEKLY,
    DAILY
}

@Preview
@Composable
fun StatsPreview() {
    val navController = rememberNavController()
    val goTo = AppNavigation(navController)
    Stats(goTo, DummyBeaglStats())
}