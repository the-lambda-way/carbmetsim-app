package edu.uwm.cs.cs595.carbmetsimapp.exerciseItem

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ExerciseItemDao {
    @Query("SELECT * FROM exercise_item_table ORDER BY uid ASC")
    fun readAllData(): Flow<List<ExerciseItem>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addExerciseItem(exerciseItem: ExerciseItem)
}