package edu.uwm.cs.cs595.carbmetsimapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApplication: Application() {
    companion object{
        var usrAge = 0
        var usrWeight = 0.0
        var usrGender = 0
        var usrFitness = 0.0
    }

}