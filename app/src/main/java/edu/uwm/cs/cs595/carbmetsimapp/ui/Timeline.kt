package edu.uwm.cs.cs595.carbmetsimapp.ui

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AddCircleOutline
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.compose.rememberNavController
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.charts.CombinedChart.DrawOrder
import com.github.mikephil.charting.components.*
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItem
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.SampleSim
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppBottomNavigation
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppNavigation


@Composable
fun Timeline(
    goTo: AppNavigation,
    foodModel: FoodItemViewModel,
    exerciseModel: ExerciseItemViewModel,
    eventModel: EventItemViewModel
) {
    val foodItemList: List<FoodItem>? by foodModel.readAllData.observeAsState()
    val exerciseItemList: List<ExerciseItem>? by exerciseModel.readAllData.observeAsState()
    val eventItemList: List<EventItem>? by eventModel.readAllData.observeAsState()

    var foodInput = SampleSim().getFoodInput(foodItemList)
    var exerciseInput = SampleSim().getExerciseInput(exerciseItemList)
    var foodEvents = SampleSim().getFoodEvents(eventItemList)
    var exerciseEvents = SampleSim().getExerciseEvents(eventItemList)





    val data = SampleSim().runNormalSample(foodInput, exerciseInput, exerciseEvents, foodEvents)
    //timelineSample()
    var titleBarText = "BeaGLcast"
    var nextEvent = ""//""Next Event: Dinner 6:00pm"
    Scaffold(
        topBar    = { TopAppBar(title = {Text(text = titleBarText)}) /*, onBackButton = goTo.lastScreen) */},
        bottomBar = { AppBottomNavigation(goTo) }
    ) {
        Column(
            //modifier = Modifier.padding(16.dp)
        ) {
            Column( modifier = Modifier.padding(16.dp, 10.dp, 16.dp, 0.dp) ) {
                Row() {
                    Box (Modifier.clip(RoundedCornerShape(50)).background(colorResource(id = R.color.purple_700))) {Text ( buildAnnotatedString {
                        withStyle(SpanStyle (color = Color.White,
                                                  fontWeight = FontWeight.Black)
                        ) { append("  Hourly  ") }
                    })}
                    Box (modifier = Modifier.padding(10.dp, 0.dp, 10.dp, 0.dp)) {Text ( "  Daily  ", Modifier.border(width = Dp(1f), color = colorResource(R.color.purple_700), shape = CircleShape), color = colorResource(R.color.purple_700), fontWeight = FontWeight.Black)}
                    Text ( "  Weekly  ", Modifier.border(width = Dp(1f), color = colorResource(R.color.purple_700), shape = CircleShape), color = colorResource(R.color.purple_700), fontWeight = FontWeight.Black)
                }
            }
//            Text("BGL safe until 4:04pm" , modifier = Modifier.padding(16.dp, 10.dp, 16.dp, 0.dp), color = Color.DarkGray)
            Box() {
                    MainTimelineGraph (
                        Modifier
                            .fillMaxHeight(.80f)
                            .fillMaxWidth(1f)
                            .padding(0.dp, 0.dp, 5.dp, 0.dp)
                    , data)

            }

            Text(nextEvent, fontWeight = FontWeight.Bold, color = Color.Gray, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth(1f).padding(0.dp, 5.dp, 0.dp, 0.dp))
            TextButton(onClick = goTo.addEvent) {
                Icon(Icons.Rounded.AddCircleOutline, contentDescription = "Add Event")
                Text(" " + stringResource(R.string.addEvent))
            }
        }
    }
}

//@Preview
//@Composable
//fun TimelinePreview() {
//    val navController = rememberNavController()
//    val goTo = AppNavigation(navController)
//    Timeline(goTo)
//}

@Composable
fun MainTimelineGraph( inModifier: Modifier, inData : ArrayList<Double> ) {
    val chartDataItems = mutableListOf<Entry>()

    var hour = 0f
    for (dat in inData) {
        chartDataItems.add(Entry(hour, dat.toFloat()))
        hour += 1
    }
    val chartDataSet = LineDataSet(chartDataItems.toList(), "BGL")

    val chartDangerLimits = mutableListOf<BarEntry>()
    chartDangerLimits.add(BarEntry(1f, arrayOf(70f, 180f, 1000f).toFloatArray()))
    val chartDangerSet = BarDataSet(chartDangerLimits, "")
    val barDataObj = BarData(chartDangerSet)
    barDataObj.barWidth = 10000000f
    chartDangerSet.setColors(arrayOf(colorResource(id = R.color.danger_chart).toArgb(),
                                     Color.Transparent.toArgb(),
                                     colorResource(id = R.color.danger_chart).toArgb()).toList())
    chartDangerSet.setDrawValues(false)

    // combined chart
    val bglChartView = CombinedChart(LocalContext.current)

    // add data to combined
    val combinedData = CombinedData()
    combinedData.setData(LineData(chartDataSet))
    combinedData.setData(barDataObj)

    // set axis ranges
    bglChartView.axisLeft.axisMaximum = chartDataSet.yMax + 10
    bglChartView.axisLeft.axisMinimum = 0f
    bglChartView.xAxis.axisMinimum = chartDataSet.xMin

    // no description text
    bglChartView.getDescription().setEnabled(false)

    // high / low limit lines
    val lowLimitLine = LimitLine(70f,"")
    val highLimitLine = LimitLine(250f,"")
    bglChartView.axisLeft.addLimitLine(lowLimitLine)
    bglChartView.axisLeft.addLimitLine(highLimitLine)
    bglChartView.axisLeft.setDrawLimitLinesBehindData(true)
    bglChartView.setBackgroundColor(Color.White.toArgb())
    bglChartView.setGridBackgroundColor(Color.White.toArgb())
    bglChartView.setDrawGridBackground(true)


    // graph style
    chartDataSet.setDrawCircles(false)
    chartDataSet.setDrawCircleHole(false)
    chartDataSet.lineWidth = 3f
    chartDataSet.circleRadius = 5f
    chartDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
    bglChartView.legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
    chartDataSet.valueTextSize = 16f

    // colors
    chartDataSet.setCircleColor(Color.DarkGray.toArgb())
    chartDataSet.setColor(colorResource(R.color.purple_chart).toArgb())
    chartDataSet.cubicIntensity = .2f

    bglChartView.legend.isEnabled = false
    bglChartView.setData(combinedData)
    bglChartView.setDrawOrder(
        arrayOf(
            DrawOrder.BAR, DrawOrder.LINE
        )
    )

    bglChartView.xAxis.setPosition(XAxis.XAxisPosition.BOTTOM)
    bglChartView.axisRight.isEnabled = false
    bglChartView.axisLeft.setPosition( YAxis.YAxisLabelPosition.INSIDE_CHART )
    bglChartView.axisLeft.textColor = Color.Gray.toArgb()
    bglChartView.axisLeft.textSize = 14f

    bglChartView.xAxis.valueFormatter = XAxisHourFormatter()

    // animate
    bglChartView.animateXY(1000, 1000);

    // don't forget to refresh the drawing
    bglChartView.invalidate();

    AndroidView( { bglChartView }, modifier = inModifier)
}

class XAxisHourFormatter : ValueFormatter() {
    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
        var hour = (value / 60).toInt()
        var ampm = "am"

        if (hour >= 12 ) { ampm = "pm"; if (hour > 12) hour -= 12 }


        return hour.toString() + ":" + "00" + ampm
    }
}