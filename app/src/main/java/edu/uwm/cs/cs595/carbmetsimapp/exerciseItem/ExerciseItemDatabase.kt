package edu.uwm.cs.cs595.carbmetsimapp.exerciseItem

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [ExerciseItem::class],
    version = 1,
    exportSchema = false
)
abstract class ExerciseItemDatabase : RoomDatabase(){
    abstract fun exerciseItemDao(): ExerciseItemDao
}