package edu.uwm.cs.cs595.carbmetsimapp.EventItem

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface EventItemDao {
    @Query("SELECT * FROM event_item_table ORDER BY uid ASC")
    fun readAllData(): Flow<List<EventItem>>
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addEventItem(eventItem: EventItem)
}