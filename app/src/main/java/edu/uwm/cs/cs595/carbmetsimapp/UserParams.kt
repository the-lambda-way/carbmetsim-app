package edu.uwm.cs.cs595.carbmetsimapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.databinding.ActivityMainBinding
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrAge
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrFitness
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrGender
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrWeight

@AndroidEntryPoint
class UserParams : AppCompatActivity(){

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.sampleSim.setOnClickListener {
            if( binding.PersonName.text.toString() == "" || binding.age.text.toString() == "" ||
                binding.activityLevel.text.toString() == "" || binding.weight.text.toString() == "")
            {
                Toast.makeText(applicationContext,"Please Enter Your Parameters", Toast.LENGTH_LONG).show()
            }
            else{
                usrAge = binding.age.text.toString().toInt()
                usrWeight = binding.weight.text.toString().toDouble()
                if (binding.gender.isChecked)
                    usrGender = 1
                else
                    usrGender = 0
                usrFitness = binding.activityLevel.text.toString().toDouble()
                startActivity(Intent(this, RunSim::class.java))
            }
        }
    }
}