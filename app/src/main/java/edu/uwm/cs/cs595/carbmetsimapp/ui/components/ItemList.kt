package edu.uwm.cs.cs595.carbmetsimapp.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Error
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.uwm.cs.cs595.carbmetsimapp.R


// validate should return "" if the input is valid, else return an error message
data class ItemParameterField(
    val value:         String,
    val onValueChange: (String) -> Unit,
    val validate:      (String) -> String,
    val label:         String,
    val unitLabel:     String,
    val helperText:    String = "",
    val keyboardType:  KeyboardType = KeyboardType.Text,
)

data class ItemParameterValue(
    val value:     String,
    val unitLabel: String,
    val label:     String = "",
)


fun isUniqueName(text: String): Boolean { return true }

fun validateInteger(text: String): String {
    val num = text.toIntOrNull()

    return when {
        num == null -> "must be an integer"
        num < 0     -> "must be positive"
        else        -> ""
    }
}

fun validateNumber(text: String): String {
    val num = text.toDoubleOrNull()

    return when {
        num == null -> "must be a number"
        num < 0     -> "must be positive"
        else        -> ""
    }
}

fun validatePercentage(text: String): String {
    val num = text.toIntOrNull()

    return when {
        num == null          -> "must be an integer"
        num < 0 || 100 < num -> "must be between 0 and 100"
        else                 -> ""
    }
}

fun validateName(text: String): String {
    return if (isUniqueName(text)) "" else "name already exists"
}


// TextField in Compose does not yet have helper messages. Using the techniques given in the samples.
// https://cs.android.com/androidx/platform/frameworks/support/+/androidx-main:compose/material/material/samples/src/main/java/androidx/compose/material/samples/TextFieldSamples.kt
@Composable
fun ItemParameterEdit(field: ItemParameterField) {
    var errorMsg by rememberSaveable { mutableStateOf("") }

    Column {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextField(
                value           = field.value,
                onValueChange   = {
                    field.onValueChange(it)
                    errorMsg = field.validate(it)
                },
                label           = { Text(field.label) },
                trailingIcon    = {
                    if (errorMsg.isNotEmpty())
                        { Icon(Icons.Default.Error, stringResource(R.string.error)) }
                    else
                        null
                },
                isError         = errorMsg.isNotEmpty(),
                keyboardOptions = KeyboardOptions(keyboardType = field.keyboardType),
            )
            Text(
                text     = field.unitLabel,
                fontSize = 18.sp,
                color    = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium),
                modifier = Modifier.padding(start = 4.dp)
            )
        }

        val (captionText, captionColor) =
            if (errorMsg.isNotEmpty())
                Pair(errorMsg, MaterialTheme.colors.error)
            else
                Pair(field.helperText, MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.medium))

        Text(
            text     = captionText,
            color    = captionColor,
            style    = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp)
        )
    }
}

@Preview
@Composable
fun ItemParameterEditPreview() {
    var text by remember { mutableStateOf("") }

    ItemParameterEdit(
        ItemParameterField(
            value         = text,
            onValueChange = { text = it },
            validate      = ::validateInteger,
            label         = "age",
            unitLabel     = "yrs",
    ))
}


@Composable
fun ItemEdit(
    fields: List<ItemParameterField>
) {
    Column {
        fields.forEach { ItemParameterEdit(it) }
    }
}

@Preview
@Composable
fun ItemEditPreview() {
    ItemEdit(listOf(
        ItemParameterField(
            value         = "",
            onValueChange = {},
            validate      = { "" },
            label         = "age",
            unitLabel     = "yrs",
        ),
        ItemParameterField(
            value         = "",
            onValueChange = {},
            validate      = { "" },
            label         = "fitness level",
            unitLabel     = "%",
            helperText    = "self-assessed fitness ability",
        ),
        ItemParameterField(
            value         = "",
            onValueChange = {},
            validate      = { "" },
            label         = "weight",
            unitLabel     = "kg",
        ),
    ))
}


fun itemParameterValueAbstract(value: ItemParameterValue): String {
    return value.value.toString() + value.unitLabel +
            (if (value.label.isEmpty()) "" else " " + value.label)
}

fun itemAbstract(
    field1: ItemParameterValue,
    field2: ItemParameterValue? = null,
    field3: ItemParameterValue? = null,
): String {
    val text = StringBuilder()

    text.append(itemParameterValueAbstract(field1))
    if (field2 == null) text.append("") else text.append(" • ", itemParameterValueAbstract(field2))
    if (field3 == null) text.append("") else text.append(" • ", itemParameterValueAbstract(field3))

    return text.toString()
}

@Preview
@Composable
fun ItemAbstractPreview() {
    Text(itemAbstract(
        ItemParameterValue("100", "g", ""),
        ItemParameterValue("0",   "g", "protein"),
        ItemParameterValue("50",  "g", "fat"),
    ))
}
