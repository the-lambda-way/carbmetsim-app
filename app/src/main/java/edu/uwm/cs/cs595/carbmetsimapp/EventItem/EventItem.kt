package edu.uwm.cs.cs595.carbmetsimapp.EventItem

import androidx.room.Entity
import androidx.room.PrimaryKey
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem

@Entity(tableName = "event_item_table")
data class EventItem (
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0,
    var foodItemList: String = "",
    var exerciseItemList: String = "",
    var time: Long = 0,
)