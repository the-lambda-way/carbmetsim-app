package edu.uwm.cs.cs595.carbmetsimapp.foodItem

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "food_item_table")
data class FoodItem (
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0,
    var name: String = "",
    var servingSize: Double = 0.0,
    var Rag: Double = 0.0,
    var Sag: Double = 0.0,
    var Protein: Double = 0.0,
    var Fat: Double = 0.0,
    var checked: Boolean = false,
)