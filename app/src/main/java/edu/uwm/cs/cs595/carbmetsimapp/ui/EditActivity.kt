package edu.uwm.cs.cs595.carbmetsimapp.ui

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.navigation.compose.rememberNavController
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppBottomNavigation
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppNavigation
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppTopAppBar
import java.lang.Exception
import java.lang.NumberFormatException

@Preview
@Composable
fun EditActivity(
    goTo: AppNavigation,
    foodModel: FoodItemViewModel,
    exerciseModel: ExerciseItemViewModel
) {


    var toastMsg = remember { mutableStateOf("") }
    var query_type = remember { mutableStateOf(AddChipType.FOOD) }
    val foodName = remember {  mutableStateOf("")  }
    val exerciseName = remember {  mutableStateOf("")  }
    val servingSize = remember {  mutableStateOf("")  }
    val RAG = remember {  mutableStateOf("")  }
    val SAG = remember {  mutableStateOf("")  }
    val protein = remember {  mutableStateOf("")  }
    val fat = remember {  mutableStateOf("")  }
    val intensity = remember {  mutableStateOf("")  }
    val duration = remember {  mutableStateOf("")  }
    Scaffold(
        topBar = { AppTopAppBar(title = R.string.editActivity, onBackButton = goTo.lastScreen) },
        bottomBar = {
            Column(
                modifier = Modifier.padding(top = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)

                ) {
                    Button( onClick = {  // clear button
                        foodName.value = ""
                        exerciseName.value = ""
                        servingSize.value = ""
                        RAG.value = ""
                        SAG.value = ""
                        protein.value = ""
                        fat.value = ""
                        intensity.value = ""
                        duration.value = ""
                    },
                        modifier = Modifier
                            .fillMaxWidth(0.5f)
                            .padding(start = 8.dp, top = 8.dp, bottom = 8.dp, end = 4.dp)
                    ){
                        Text(stringResource(R.string.clear))
                    }
                    Button(onClick = { // save button
                        if (query_type.component1() == AddChipType.FOOD) {
                            var succ : Boolean = true;
                            try {
                                foodModel.addFoodItem(FoodItem(
                                    name = foodName.value,
                                    servingSize = servingSize.value.toDouble(),
                                    Rag = RAG.value.toDouble(),
                                    Sag = SAG.value.toDouble(),
                                    Protein = protein.value.toDouble(),
                                    Fat = fat.value.toDouble()
                                ))
                            }
                            catch (e : NumberFormatException) {
                                toastMsg.value = "Error. Check numbers."
                                succ = false
                                e.printStackTrace()
                            }
                            catch (e : Exception) {
                                toastMsg.value = "Error."
                                succ = false
                                e.printStackTrace()
                            }

                            if ( succ )
                                toastMsg.value = "Saved. Press back."

                        }
                        if (query_type.component1() == AddChipType.EXERCISE) {
                            var succ = true
                            try {
                                exerciseModel.addExerciseItem(ExerciseItem(
                                    name = exerciseName.value,
                                    duration = duration.value.toDouble(),
                                    Intensity = intensity.value.toDouble()
                                ))
                            }
                            catch (e : NumberFormatException) {
                                toastMsg.value = "Check your numbers"
                                succ = false
                            }
                            catch (e : Exception) {
                                succ = false
                                toastMsg.value = "Error"
                            }

                            if (succ)
                                toastMsg.value = "Saved. Press back."

                        }
                        goTo.devMenu
                    },
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 4.dp, top = 8.dp, bottom = 8.dp, end = 8.dp)
                    ){
                        Text(stringResource(R.string.saveItem))
                    }
                }
                AppBottomNavigation(goTo)
            }
        }
    )
    {
        Column {
            Row(
                verticalAlignment = Alignment.Bottom,
                modifier = Modifier.padding(8.dp),
            ) {
                Column ( ){
                    Row(
                        modifier = Modifier
                            .padding(8.dp)
                    ) {
                        @Composable
                        fun decideColor (type : AddChipType) : Color {
                            return (if (type == query_type.component1()) colorResource(id = R.color.selected_button_color) else colorResource(
                                id = R.color.purple_500)
                                    ) }
                        AddChip(
                            category = "Food",
                            selColor = decideColor(AddChipType.FOOD),
                            onChangeFun = { query_type.value = AddChipType.FOOD })
                        AddChip(
                            category = "Exercise",
                            selColor = decideColor(AddChipType.EXERCISE),
                            onChangeFun = { query_type.value = AddChipType.EXERCISE })
                    }
                    if (query_type.component1() == AddChipType.FOOD){
                        OutlinedTextField(
                            value = foodName.value,
                            onValueChange = { foodName.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Food Name")}
                        )
                        OutlinedTextField(
                            value = servingSize.value,
                            onValueChange = { servingSize.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Serving Size")}
                        )
                        OutlinedTextField(
                            value = RAG.value,
                            onValueChange = { RAG.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("RAG")}
                        )
                        OutlinedTextField(
                            value = SAG.value,
                            onValueChange = { SAG.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("SAG")}
                        )
                        OutlinedTextField(
                            value = protein.value,
                            onValueChange = { protein.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Protein")}
                        )
                        OutlinedTextField(
                            value = fat.value,
                            onValueChange = { fat.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Fat")}
                        )
                    }
                    if (query_type.component1() == AddChipType.EXERCISE){
                        OutlinedTextField(
                            value = exerciseName.value,
                            onValueChange = { exerciseName.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Exercise Name")}
                        )
                        OutlinedTextField(
                            value = intensity.value,
                            onValueChange = { intensity.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Intensity")}
                        )
                        OutlinedTextField(
                            value = duration.value,
                            onValueChange = { duration.value = it },

                            Modifier
                                .fillMaxWidth()
                                .padding(vertical = 0.dp),
                            singleLine = true,
                            maxLines = 1,
                            label = { Text("Exercise Duration")}
                        )
                    }
                }
            }
        }
    }
    if ( toastMsg.component1().isNotBlank() ) {
        Toast.makeText(LocalContext.current, toastMsg.component1(), Toast.LENGTH_LONG).show()
        toastMsg.value = ""
    }
}


@Composable
fun AddChip(category: String, selColor: Color, isSelected: Boolean = false, onChangeFun: () -> Unit){
    Surface (
        modifier = Modifier.padding(end = 8.dp),
        elevation = 8.dp,
        shape = MaterialTheme.shapes.medium,
        color = MaterialTheme.colors.primary
    ){
        Column(
            modifier = Modifier.padding(1.dp)
        ) {
            Row(
                modifier = Modifier
                    .toggleable(
                        value = isSelected,
                        onValueChange = {}
                    )
            ) {
                Button(
                    /*style = MaterialTheme.typography.body2,*/
                    /*color = Color.White,*/
                    colors = ButtonDefaults.buttonColors(backgroundColor = selColor),
                    onClick = onChangeFun
                ) { Text(category) }
            }

        }
    }
}

enum class AddChipType {
    FOOD,
    EXERCISE
}

//@Preview
//@Composable
//fun EditActivityPreview() {
//    val navController = rememberNavController()
//    val goTo = AppNavigation(navController)
//    EditActivity(goTo)
//}
