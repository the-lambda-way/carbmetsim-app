package edu.uwm.cs.cs595.carbmetsimapp.EventItem

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [EventItem::class],
    version = 1,
    exportSchema = false
)
abstract class EventItemDatabase : RoomDatabase(){
    abstract fun eventItemDao(): EventItemDao
}