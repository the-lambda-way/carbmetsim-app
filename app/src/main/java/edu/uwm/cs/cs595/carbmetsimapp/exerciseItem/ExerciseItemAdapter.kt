package edu.uwm.cs.cs595.carbmetsimapp.exerciseItem

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.uwm.cs.cs595.carbmetsimapp.databinding.EventDesignBinding
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem

class ExerciseItemAdapter: RecyclerView.Adapter<ExerciseItemAdapter.MyViewHolder>(){

    var exerciseItemList = emptyList<ExerciseItem>()

    class MyViewHolder(val binding: EventDesignBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val viewHolder = MyViewHolder(
            EventDesignBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.binding.addFood.setOnClickListener{
            if (viewHolder.binding.addFood.isChecked){
                val name = viewHolder.binding.foodEvent.text.toString()
                val exercise: ExerciseItem? = exerciseItemList.find {it.name == name}
                if (exercise != null) {
                    exercise.checked = true
                }
            }
            else{
                val name = viewHolder.binding.foodEvent.text.toString()
                val exercise: ExerciseItem? = exerciseItemList.find {it.name == name}
                if (exercise != null) {
                    exercise.checked = false
                }
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.foodEvent.text = exerciseItemList[position].name
    }

    override fun getItemCount(): Int {
        return exerciseItemList.size
    }

    fun setData(exerciseItem: List<ExerciseItem>){
        exerciseItemList = exerciseItem
        notifyDataSetChanged()
    }
}