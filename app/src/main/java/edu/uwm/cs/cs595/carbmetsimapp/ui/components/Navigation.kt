package edu.uwm.cs.cs595.carbmetsimapp.ui.components

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.BarChart
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Timeline
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavHostController
import androidx.navigation.compose.*
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.model.DummyBeaglStats
import edu.uwm.cs.cs595.carbmetsimapp.ui.*


class AppNavigation(val navController: NavHostController) {
    val devMenu: () -> Unit = {
        navController.navigate("devMenu") {
            // Pop up to the start destination of the graph to
            // avoid building up a large stack of destinations
            // on the back stack as users select items
            popUpTo = navController.graph.startDestination

            // Avoid multiple copies of the same destination when
            // reselecting the same item
            launchSingleTop = true
        }
    }

    // Main pages
    val timeline: () -> Unit = {
        navController.navigate("timeline") {
            popUpTo = navController.graph.startDestination
            launchSingleTop = true
        }
    }

    val settings: () -> Unit = {
        navController.navigate("settings") {
            popUpTo = navController.graph.startDestination
            launchSingleTop = true
        }
    }

    val stats: () -> Unit = {
        navController.navigate("stats") {
            popUpTo = navController.graph.startDestination
            launchSingleTop = true
        }
    }

    // Subpages
    val addEvent:     () -> Unit = { navController.navigate("addEvent") }
    val editActivity: () -> Unit = {
        navController.navigate("editActivity") {
            popUpTo = navController.graph.startDestination
            launchSingleTop = true
        }
    }
    val lastScreen:   () -> Unit = { navController.popBackStack() }
}

@Composable
fun AppNavigationHost(
    foodItemViewModel: FoodItemViewModel,
    exerciseItemViewModel: ExerciseItemViewModel,
    eventItemViewModel: EventItemViewModel
) {
    val navController = rememberNavController()
    val goTo = AppNavigation(navController)

    NavHost(navController, startDestination = "timeline") {
        composable("devMenu")      { DevMenu(goTo) }
        composable("timeline")     { Timeline(goTo, foodItemViewModel, exerciseItemViewModel, eventItemViewModel) }
        composable("addEvent")     { AddEvent(goTo, foodItemViewModel, exerciseItemViewModel, eventItemViewModel) }
        composable("editActivity") { EditActivity(goTo, foodItemViewModel, exerciseItemViewModel) }
        composable("settings")     { Settings(goTo) }
        composable("stats")        { Stats(goTo, DummyBeaglStats()) }
    }
}


@Composable
fun AppBottomNavigation(goTo: AppNavigation) {
    BottomNavigation {
        val navBackStackEntry by goTo.navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.arguments?.getString(KEY_ROUTE)

//        BottomNavigationItem(
//            icon     = { Icon(Icons.Filled.Favorite, null) },
//            label    = { Text(stringResource(R.string.devMenu)) },
//            selected = currentRoute == stringResource(R.string.devMenu),
//            onClick  = goTo.devMenu
//        )

        BottomNavigationItem(
            icon     = { Icon(Icons.Filled.Settings, null) },
            label    = { Text(stringResource(R.string.settings)) },
            selected = currentRoute == stringResource(R.string.settings),
            onClick  = goTo.settings
        )

        BottomNavigationItem(
            icon     = { Icon(Icons.Filled.Timeline, null) },
            label    = { Text(stringResource(R.string.timeline)) },
            selected = currentRoute == stringResource(R.string.timeline),
            onClick  = goTo.timeline
        )

        BottomNavigationItem(
            icon     = { Icon(Icons.Filled.BarChart, null) },
            label    = { Text(stringResource(R.string.stats)) },
            selected = currentRoute == stringResource(R.string.stats),
            onClick  = goTo.stats
        )
    }
}

@Preview
@Composable
fun AppBottomNavigationPreview() {
    val navController = rememberNavController()
    val goTo = AppNavigation(navController)
    AppBottomNavigation(goTo)
}