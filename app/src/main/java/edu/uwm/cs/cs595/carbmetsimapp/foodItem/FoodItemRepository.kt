package edu.uwm.cs.cs595.carbmetsimapp.foodItem

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FoodItemRepository @Inject constructor(
    private val foodItemDao: FoodItemDao
) {
    fun readAllData(): Flow<List<FoodItem>> {
        return foodItemDao.readAllData()
    }
    suspend fun addFoodItem(foodItem: FoodItem){
        foodItemDao.addFoodItem(foodItem)
    }
}