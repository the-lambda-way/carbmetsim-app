package edu.uwm.cs.cs595.carbmetsimapp.ui

import androidx.compose.runtime.*
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.*


@Composable
fun EditItem(
    onCloseButton: () -> Unit,
    onSaveButton: () -> Unit,
    fields: List<ItemParameterField>,
    saveEnabled: Boolean = false,
) {
    FullScreenDialog(R.string.editItem, onCloseButton, onSaveButton, saveEnabled) {
        ItemEdit(fields)
    }
}

@Preview
@Composable
fun EditItemPreview() {
    var age          by remember { mutableStateOf("") }
    var fitnessLevel by remember { mutableStateOf("") }
    var weight       by remember { mutableStateOf("") }

    val updateAge:          (String) -> Unit = { age = it }
    val updateFitnessLevel: (String) -> Unit = { fitnessLevel = it }
    val updateWeight:       (String) -> Unit = { weight = it }

    val fields = listOf(
        ItemParameterField(
            value         = age,
            onValueChange = updateAge,
            validate      = ::validateInteger,
            label         = "age",
            unitLabel     = "yrs",
            keyboardType  = KeyboardType.Number,
        ),
        ItemParameterField(
            value         = fitnessLevel,
            onValueChange = updateFitnessLevel,
            validate      = ::validatePercentage,
            label         = "fitness level",
            unitLabel     = "%",
            helperText    = "self-assessed fitness ability",
            keyboardType  = KeyboardType.Number,
        ),
        ItemParameterField(
            value         = weight,
            onValueChange = updateWeight,
            validate      = ::validateNumber,
            label         = "weight",
            unitLabel     = "kg",
            keyboardType  = KeyboardType.Number,
        ),
    )

    EditItem(
        onCloseButton = {},
        onSaveButton  = {},
        fields        = fields,
    )
}
