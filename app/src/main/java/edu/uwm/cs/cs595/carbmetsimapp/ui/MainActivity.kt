package edu.uwm.cs.cs595.carbmetsimapp.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.AppNavigationHost


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val foodItemViewModel: FoodItemViewModel by viewModels()
    private val foodItemAdapter: FoodItemAdapter by lazy { FoodItemAdapter() }
    private val exerciseItemViewModel: ExerciseItemViewModel by viewModels()
    private val exerciseItemAdapter: ExerciseItemAdapter by lazy { ExerciseItemAdapter() }
    private val eventItemViewModel: EventItemViewModel by viewModels()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        foodItemViewModel.readAllData.observe(this, {
            foodItemAdapter.setData(it)
        })

        setContent {
            MaterialTheme {
                AppNavigationHost(foodItemViewModel = foodItemViewModel, exerciseItemViewModel = exerciseItemViewModel, eventItemViewModel = eventItemViewModel)
            }
        }
    }

    companion object {
        // Used to load the 'carbmetlib' library on application startup.
        init {
            System.loadLibrary("carbmetlib")
        }
    }
}
