package edu.uwm.cs.cs595.carbmetsimapp.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.buttons
import com.vanpra.composematerialdialogs.datetime.datepicker
import com.vanpra.composematerialdialogs.datetime.timepicker.TimePickerColors
import com.vanpra.composematerialdialogs.datetime.timepicker.TimePickerDefaults
import com.vanpra.composematerialdialogs.datetime.timepicker.timepicker
import edu.uwm.cs.cs595.carbmetsimapp.R
import java.time.LocalDate
import java.time.LocalTime


@Composable
fun FullScreenDialog(
    @StringRes title: Int,
    onCloseButton: () -> Unit,
    onSaveButton: () -> Unit,
    saveEnabled: Boolean = false,
    content: @Composable () -> Unit
) {
    Scaffold(
        topBar  = { AppDialogTopAppBar(title, onCloseButton, onSaveButton, saveEnabled) },
        content = {
            Surface(
                modifier = Modifier.padding(16.dp),
                content  = content
            )
        }
    )
}

@Preview
@Composable
fun FullScreenDialogPreview() {
    FullScreenDialog(
        title         = R.string.editItem,
        onCloseButton = {},
        onSaveButton  = {},
    ) {
        Text("body")
    }
}


@Composable
fun DatePickerDialogBox(
    onComplete: (LocalDate) -> Unit,
    controller: @Composable (openDialog: ()-> Unit) -> Unit
) {
    val dialog = remember { MaterialDialog() }

    dialog.build(content = {
        datepicker(onComplete = onComplete)

        buttons {
            positiveButton("Ok")
            negativeButton("Cancel")
        }
    })

    controller { dialog.show() }
}

@Preview
@Composable
fun DatePickerDialogBoxPreview() {
    var date by remember { mutableStateOf(LocalDate.now()) }

    Scaffold {
        DatePickerDialogBox(
            onComplete = { date = it }
        ) { openDialog ->
            DateButton(date, onClick = openDialog)
        }
    }
}


@Composable
fun TimePickerDialogBox(
    onComplete: (LocalTime) -> Unit,
    colors: TimePickerColors = TimePickerDefaults.colors(),
    controller: @Composable (openDialog: ()-> Unit) -> Unit
) {
    val dialog = remember { MaterialDialog() }

    dialog.build(content = {
        timepicker(colors = colors, onComplete = onComplete)

        buttons {
            positiveButton("Ok")
            negativeButton("Cancel")
        }
    })

    controller { dialog.show() }
}

@Preview
@Composable
fun TimePickerDialogBoxPreview() {
    var time by remember { mutableStateOf(LocalTime.now()) }

    Scaffold {
        TimePickerDialogBox(
            onComplete = { time = it }
        ) { openDialog ->
            TimeButton(time, onClick = openDialog)
        }
    }
}