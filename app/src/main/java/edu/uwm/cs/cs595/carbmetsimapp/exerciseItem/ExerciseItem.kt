package edu.uwm.cs.cs595.carbmetsimapp.exerciseItem

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "exercise_item_table")
data class ExerciseItem (
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0,
    var name: String = "",
    var Intensity: Double = 0.0,
    var duration: Double = 0.0,
    var checked: Boolean = false,
)