package edu.uwm.cs.cs595.carbmetsimapp.exerciseItem

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ExerciseItemRepository @Inject constructor(
    private val exerciseItemDao: ExerciseItemDao
) {

   //val readAllData: Flow<List<ExerciseItem>> = exerciseItemDao.readAllData()
    fun readAllData(): Flow<List<ExerciseItem>> {
        return exerciseItemDao.readAllData()
    }
    suspend fun addExerciseItem(exerciseItem: ExerciseItem){
        exerciseItemDao.addExerciseItem(exerciseItem)
    }
}