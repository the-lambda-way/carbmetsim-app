package edu.uwm.cs.cs595.carbmetsimapp

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*
import kotlin.collections.ArrayList


// Must be manually updated if the carbmetsim values ever change. This is because we are using static arrays. The value
// is not expected to change.
const val BLOOD_KILLBIN_SIZE = 20


// We don't want to overwrite the default values of the C++ version of Metabolic Parameters. One way to prevent this is
// to use nullable types on our internal data class and when it is marshalled to C++ not pass the null values.
// - Mike

data class BloodParams(
    var rbcBirthRate:       Double? = null,
    var glycationProbSlope: Double? = null,
    var glycationProbConst: Double? = null,
    var glycolysisMin:      Double? = null,
    var glycolysisMax:      Double? = null,
    var minGlucoseLevel:    Double? = null,
    var baseGlucoseLevel:   Double? = null,
    var highGlucoseLevel:   Double? = null,
    var highLactateLevel:   Double? = null,
    var peakInsulinLevel:   Double? = null,
    var baseInsulinLevel:   Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class BrainParams(
    var glucoseOxidized:  Double? = null,
    var glucoseToAlanine: Double? = null,
    var bAAToGlutamine:   Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class HeartParams(
    var basalGlucoseAbsorbed: Double? = null,
    var Glut4Km:              Double? = null,
    var Glut4VMAX:            Double? = null,
    var lactateOxidized:      Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class HumanParams(
    var age:                                          Double?,
    var gender:                                       Double?,
    var fitnessLevel:                                 Double?,
    var bodyWeight:                                   Double?,
    var glut4Impact:                                  Double? = null,
    var liverGlycogenSynthesisImpact:                 Double? = null,
    var glycolysisMinImpact:                          Double? = null,
    var glycolysisMaxImpact:                          Double? = null,
    var excretionKidneysImpact:                       Double? = null,
    var intensityPeakGlucoseProd:                     Double? = null,
    var gngImpact:                                    Double? = null,
    var liverGlycogenBreakdownImpact:                 Double? = null,
    var insulinImpactOnGlycolysis_Mean:               Double? = null,
    var insulinImpactOnGlycolysis_StdDev:             Double? = null,
    var insulinImpactOnGNG_Mean:                      Double? = null,
    var insulinImpactOnGNG_StdDev:                    Double? = null,
    var insulinImpactGlycogenBreakdownInLiver_Mean:   Double? = null,
    var insulinImpactGlycogenBreakdownInLiver_StdDev: Double? = null,
    var insulinImpactGlycogenSynthesisInLiver_Mean:   Double? = null,
    var insulinImpactGlycogenSynthesisInLiver_StdDev: Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class IntestineParams(
    var aminoAcidsAbsorptionRate:   Double? = null,
    var glutamineOxidationRate:     Double? = null,
    var glutamineToAlanineFraction: Double? = null,
    var Glut2Km_In:                 Double? = null,
    var Glut2VMAX_In:               Double? = null,
    var Glut2Km_Out:                Double? = null,
    var Glut2VMAX_Out:              Double? = null,
    var sglt1Rate:                  Double? = null,
    var fluidVolumeInEnterocytes:   Double? = null,
    var fluidVolumeInLumen:         Double? = null,
    var glycolysisMin:              Double? = null,
    var glycolysisMax:              Double? = null,
    var RAG_Mean:                   Double? = null,
    var RAG_StdDev:                 Double? = null,
    var SAG_Mean:                   Double? = null,
    var SAG_StdDev:                 Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class KidneysParams(
    var glycolysisMin: Double? = null,
    var glycolysisMax: Double? = null,
    var gngKidneys:    Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class LiverParams(
    var glycogen:                    Double? = null,
    var glycogenMax:                 Double? = null,
    var glycogenToGlucoseInLiver:    Double? = null,
    var glucoseToGlycogenInLiver:    Double? = null,
    var maxLipogenesis:              Double? = null,
    var glycolysisMin:               Double? = null,
    var glycolysisMax:               Double? = null,
    var glycolysisToLactateFraction: Double? = null,
    var gngLiver:                    Double? = null,
    var glucoseToNEFA:               Double? = null,
    var fluidVolume:                 Double? = null,
    var Glut2Km:                     Double? = null,
    var Glut2VMAX:                   Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class MusclesParams(
    var glycogen:                           Double? = null,
    var glycogenMax:                        Double? = null,
    var glycogenShareDuringExerciseMean:    Double? = null,
    var basalGlucoseAbsorbed:               Double? = null,
    var maxGlucoseAbsorptionDuringExercise: Double? = null,
    var baaToGlutamine:                     Double? = null,
    var glycolysisMin:                      Double? = null,
    var glycolysisMax:                      Double? = null,
    var glucoseToGlycogen:                  Double? = null,
    var Glut4Km:                            Double? = null,
    var Glut4VMAX:                          Double? = null,
    var peakGlut4VMAX:                      Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class PortalVeinParams(
    var fluidVolume: Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class StomachParams(
    var geConstant: Double? = null,
    var geSlopeMin: Double? = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class MetabolicParams(
    var blood:      BloodParams?      = null,
    var brain:      BrainParams?      = null,
    var heart:      HeartParams?      = null,
    var body:       HumanParams?      = null,
    var intestine:  IntestineParams?  = null,
    var kidneys:    KidneysParams?    = null,
    var liver:      LiverParams?      = null,
    var muscles:    MusclesParams?    = null,
    var portalVein: PortalVeinParams? = null,
    var stomach:    StomachParams?    = null,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class FoodType(
    var foodID:      Int    = 0,
    var name:        String = "",
    var servingSize: Double = 0.0,
    var RAG:         Double = 0.0,
    var SAG:         Double = 0.0,
    var protein:     Double = 0.0,
    var fat:         Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class ExerciseType(
    var exerciseID: Int    = 0,
    var name:       String = "",
    var intensity:  Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

enum class BodyState {FED_RESTING, FED_EXERCISING, POSTABSORPTIVE_RESTING, POSTABSORPTIVE_EXERCISING;
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

enum class EventType {FOOD, EXERCISE, HALT;
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

open class Event(open var fireTime: Int, var eventType: EventType)
{
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class FoodEvent(
    override var fireTime: Int = 0,
    var          quantity: Int = 0,
    var          foodID:   Int = 0,
)
: Event(fireTime, EventType.FOOD)
{
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class ExerciseEvent(
    override var fireTime:   Int = 0,
    var          duration:   Int = 0,
    var          exerciseID: Int = 0,
)
: Event(fireTime, EventType.EXERCISE)
{
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class HaltEvent(
    override var fireTime: Int = 0,
)
: Event(fireTime, EventType.HALT)
{
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class GlucoseState(
    var amount:      Double = 0.0,
    var bloodBGL:    Double = 0.0,
    var bloodMinBGL: Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class FatState(
    var amount:     Double = 0.0,
    var bodyWeight: Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class TotalsState(
    var glycolysis:               Double = 0.0,
    var excretion:                Double = 0.0,
    var oxidation:                Double = 0.0,
    var GNG:                      Double = 0.0,
    var liverGlycogenStorage:     Double = 0.0,
    var liverGlycogenBreakdown:   Double = 0.0,
    var musclesGlycogenStorage:   Double = 0.0,
    var musclesGlycogenBreakdown: Double = 0.0,
    var glucoseFromIntestine:     Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class ChymeConsumed(
    var RAG:         Double = 0.0,
    var SAG:         Double = 0.0,
    var origRAG:     Double = 0.0,
    var origSAG:     Double = 0.0,
    var RAGConsumed: Double = 0.0,
    var SAGConsumed: Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class GlycogenSynthesisState(
    var glycogen: Double = 0.0,
    var glucose:  Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class GluconeogenesisState(
    var glucose:      Double = 0.0,
    var glycogen:     Double = 0.0,
    var bloodGlucose: Double = 0.0,
    var bloodLactate: Double = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Blood(
    var getBGL:               Double      = 0.0,
    var baseBGL:              Double      = 0.0,
    var volume:               Double      = 0.0,
    var minGlucoseLevel:      Double      = 0.0,
    var glucose:              Double      = 0.0,
    var baseInsulinLevel:     Double      = 0.0,
    var insulinLevel:         Double      = 0.0,
    var lactate:              Double      = 0.0,
    var branchedAminoAcids:   Double      = 0.0,
    var glutamine:            Double      = 0.0,
    var alanine:              Double      = 0.0,
    var unbranchedAminoAcids: Double      = 0.0,
    var gngSubstrates:        Double      = 0.0,
    var glycolysisPerTick:    Double      = 0.0,
    var totalGlycolysisSoFar: Double      = 0.0,
    var killRate:             DoubleArray = DoubleArray(BLOOD_KILLBIN_SIZE),
    var killBin:              IntArray    = IntArray(BLOOD_KILLBIN_SIZE),
    var RBCsUpdated:          Boolean     = false,
    var glLumen:              Double      = 0.0,
    var glEnterocytes:        Double      = 0.0,
    var glPortalVein:         Double      = 0.0,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Blood

        if (getBGL != other.getBGL) return false
        if (baseBGL != other.baseBGL) return false
        if (volume != other.volume) return false
        if (minGlucoseLevel != other.minGlucoseLevel) return false
        if (glucose != other.glucose) return false
        if (baseInsulinLevel != other.baseInsulinLevel) return false
        if (insulinLevel != other.insulinLevel) return false
        if (lactate != other.lactate) return false
        if (branchedAminoAcids != other.branchedAminoAcids) return false
        if (glutamine != other.glutamine) return false
        if (alanine != other.alanine) return false
        if (unbranchedAminoAcids != other.unbranchedAminoAcids) return false
        if (gngSubstrates != other.gngSubstrates) return false
        if (glycolysisPerTick != other.glycolysisPerTick) return false
        if (totalGlycolysisSoFar != other.totalGlycolysisSoFar) return false
        if (!killRate.contentEquals(other.killRate)) return false
        if (!killBin.contentEquals(other.killBin)) return false
        if (RBCsUpdated != other.RBCsUpdated) return false
        if (glLumen != other.glLumen) return false
        if (glEnterocytes != other.glEnterocytes) return false
        if (glPortalVein != other.glPortalVein) return false

        return true
    }

    override fun hashCode(): Int {
        var result = getBGL.hashCode()
        result = 31 * result + baseBGL.hashCode()
        result = 31 * result + volume.hashCode()
        result = 31 * result + minGlucoseLevel.hashCode()
        result = 31 * result + glucose.hashCode()
        result = 31 * result + baseInsulinLevel.hashCode()
        result = 31 * result + insulinLevel.hashCode()
        result = 31 * result + lactate.hashCode()
        result = 31 * result + branchedAminoAcids.hashCode()
        result = 31 * result + glutamine.hashCode()
        result = 31 * result + alanine.hashCode()
        result = 31 * result + unbranchedAminoAcids.hashCode()
        result = 31 * result + gngSubstrates.hashCode()
        result = 31 * result + glycolysisPerTick.hashCode()
        result = 31 * result + totalGlycolysisSoFar.hashCode()
        result = 31 * result + killRate.contentHashCode()
        result = 31 * result + killBin.contentHashCode()
        result = 31 * result + RBCsUpdated.hashCode()
        result = 31 * result + glLumen.hashCode()
        result = 31 * result + glEnterocytes.hashCode()
        result = 31 * result + glPortalVein.hashCode()
        return result
    }

    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Brain(
    var oxidationPerTick: Double       = 0.0,
    var glucoseRemoved:   GlucoseState = GlucoseState(),
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Heart(
    var oxidationPerTick: Double       = 0.0,
    var basalAbsorption:  GlucoseState = GlucoseState(),
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class HumanBody(
    var bodyState:                    BodyState = BodyState.FED_RESTING,
    var glut4Impact:                  Double    = 0.0,
    var liverGlycogenSynthesisImpact: Double    = 0.0,
    var excretionKidneysImpact:       Double    = 0.0,
    var age:                          Int       = 0,
    var gender:                       Int       = 0,
    var fitnessLevel:                 Int       = 0,
    var VO2Max:                       Double    = 0.0,
    var percentVO2Max:                Double    = 0.0,
    var bodyWeight:                   Double    = 0.0,
    var fatFraction:                  Double    = 0.0,
    var exerciseOverAt:               Int       = 0,
    var lastHardExerciseAt:           Int       = 0,
    var isExercising:                 Boolean   = false,

    var totalGlycolysisSoFar:                Double      = 0.0,
    var totalGNGSoFar:                       Double      = 0.0,
    var totalOxidationSoFar:                 Double      = 0.0,
    var totalLiverGlycogenStorageSoFar:      Double      = 0.0,
    var totalLiverGlycogenBreakdownSoFar:    Double      = 0.0,
    var totalMusclesGlycogenStorageSoFar:    Double      = 0.0,
    var totalMusclesGlycogenBreakdownSoFar:  Double      = 0.0,
    var totalEndogeneousGlucoseReleaseSoFar: Double      = 0.0,
    var totalGlucoseReleaseSoFar:            Double      = 0.0,
    var getTotals:                           TotalsState = TotalsState(),

    var tempGNG:                  Double = 0.0,
    var tempGlycolysis:           Double = 0.0,
    var tempOxidation:            Double = 0.0,
    var tempExcretion:            Double = 0.0,
    var tempGlycogenStorage:      Double = 0.0,
    var tempGlycogenBreakdown:    Double = 0.0,
    var baseBGL:                  Double = 0.0,
    var peakBGL:                  Double = 0.0,
    var intensityPeakGlucoseProd: Double = 0.0,
    var insulinImpactOnGNG_Mean:  Double = 0.0,
    var insulinImpactGlycogenBreakdownInLiver_Mean: Double = 0.0,

    var dayEndTotals:                          TotalsState = TotalsState(),
    var totalGlycolysisPerTick:                Double      = 0.0,
    var totalGNGPerTick:                       Double      = 0.0,
    var totalOxidationPerTick:                 Double      = 0.0,
    var totalGlycogenStoragePerTick:           Double      = 0.0,
    var totalGlycogenBreakdownPerTick:         Double      = 0.0,
    var totalEndogeneousGlucoseReleasePerTick: Double      = 0.0,
    var totalGlucoseReleasePerTick:            Double      = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Intestine(
    var glycolysisPerTick:         Double       = 0.0,
    var toPortalVeinPerTick:       Double       = 0.0,
    var chymeConsumed:             ArrayList<ChymeConsumed> = ArrayList(),
    var totalRAGConsumed:          Double       = 0.0,
    var totalSAGConsumed:          Double       = 0.0,
    var activeAbsorption:          Double       = 0.0,
    var passiveAbsorption:         Double       = 0.0,
    var glLumen:                   Double       = 0.0,
    var glEnterocytes:             Double       = 0.0,
    var glPortalVein:              Double       = 0.0,
    var glPortalVeinConcentration: Double       = 0.0,
    var glucoseFromBlood:          GlucoseState = GlucoseState(),
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Kidneys(
    var totalExcretion:        Double       = 0.0,
    var glycolysisPerTick:     Double       = 0.0,
    var gngPerTick:            Double       = 0.0,
    var excretionPerTick:      Double       = 0.0,
    var toGlycolysis:          Double       = 0.0,
    var postGlycolysis:        GlucoseState = GlucoseState(),
    var postGluconeogenesis:   GlucoseState = GlucoseState(),
    var postGlucoseExtraction: GlucoseState = GlucoseState(),
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Liver(
    var glycogen:                Double  = 0.0,
    var fluidVolume:             Double  = 0.0,
    var glucose:                 Double  = 0.0,
    var absorptionPerTick:       Double  = 0.0,
    var toGlycogenPerTick:       Double  = 0.0,
    var fromGlycogenPerTick:     Double  = 0.0,
    var glycolysisPerTick:       Double  = 0.0,
    var gngPerTick:              Double  = 0.0,
    var releasePerTick:          Double  = 0.0,
    var excessGlucoseAbsorption: Double  = 0.0,
    var glucoseNeeded:           Double  = 0.0,
    var maxGNGDuringExercise:    Double  = 0.0,
    var lipogenesisOccurred:     Boolean = false,
    var postGlycogen:            GlycogenSynthesisState = GlycogenSynthesisState(),
    var postGluconeogenesis:     GluconeogenesisState   = GluconeogenesisState(),
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Muscles(
    var glucoseAbsorbedPerTick:     Double       = 0.0,
    var glycogenSynthesizedPerTick: Double       = 0.0,
    var glycogenBreakdownPerTick:   Double       = 0.0,
    var oxidationPerTick:           Double       = 0.0,
    var glycogenOxidizedPerTick:    Double       = 0.0,
    var glycolysisPerTick:          Double       = 0.0,
    var hadExercise:                Boolean      = false,
    var absorbedFromBlood:          Double       = 0.0,
    var absorptionState:            GlucoseState = GlucoseState(),
    var glycogenShare:              Double       = 0.0,
    var basalBase:                  GlucoseState = GlucoseState(),
    var basalGLUT4:                 GlucoseState = GlucoseState(),
    var basalGLUT4Occurred:         Boolean      = false,
    var glycogen:                   Double       = 0.0,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class PortalVein(
    var getConcentration: Double       = 0.0,
    var fromBlood:        GlucoseState = GlucoseState(),
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}

data class Stomach(
    var RAG:                Double   = 0.0,
    var SAG:                Double   = 0.0,
    var protein:            Double   = 0.0,
    var fat:                Double   = 0.0,
    var totalFood:          Double   = 0.0,
    var calorificDensity:   Double   = 0.0,
    var geSlope:            Double   = 0.0,
    var ragInBolus:         Double   = 0.0,
    var sagInBolus:         Double   = 0.0,
    var proteinInBolus:     Double   = 0.0,
    var fatInBolus:         FatState = FatState(),
    var stomachEmpty:       Boolean  = false,
    var stomachBecameEmpty: Boolean  = false,
){
    companion object
    {
        private external fun onClassInit()
        init { onClassInit() }
    }
}
