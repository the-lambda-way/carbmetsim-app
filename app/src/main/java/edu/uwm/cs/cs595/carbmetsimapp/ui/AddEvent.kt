package edu.uwm.cs.cs595.carbmetsimapp.ui

import android.preference.PreferenceManager
import android.provider.CalendarContract
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.colorspace.WhitePoint
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItem
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.R
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.ui.components.*
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneOffset


@Composable
fun AddEvent(
    goTo: AppNavigation,
    foodModel: FoodItemViewModel,
    exerciseModel: ExerciseItemViewModel,
    eventModel: EventItemViewModel
    ) {

    val foodItemList: List<FoodItem>? by foodModel.readAllData.observeAsState()
    val exerciseItemList: List<ExerciseItem>? by exerciseModel.readAllData.observeAsState()

    val amount = remember {  mutableStateOf("")  }
    var query = remember {  mutableStateOf("")  }
    var query_type = remember { mutableStateOf(EventChipType.ALL) }
    var date by remember { mutableStateOf(LocalDate.now()) }
    var time by remember { mutableStateOf(LocalTime.now()) }
    var offset by remember { mutableStateOf(0f) }


    Scaffold(
        topBar    = { AppTopAppBar(title = R.string.addEvent, onBackButton = goTo.lastScreen) },
        bottomBar = {
            Column(
                modifier = Modifier.padding(top = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)

                ) {
                    Button(onClick = {}, //goTo.editActivity,
                        modifier = Modifier
                            .fillMaxWidth(0.5f)
                            .padding(start = 8.dp, top = 8.dp, bottom = 8.dp, end = 4.dp)
                    ){
                        Text(stringResource(R.string.newActivity))
                    }
                    Button(onClick = goTo.editActivity,
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 4.dp, top = 8.dp, bottom = 8.dp, end = 8.dp)
                    ){
                        Text(stringResource(R.string.newItem))
                    }
                }
                AppBottomNavigation(goTo)
            }
        }
    ) {
        Column {
            Row(
                verticalAlignment = Alignment.Bottom,
                modifier = Modifier.padding(8.dp),
            ) {
                Column() {
                    OutlinedTextField(
                        value = amount.value,
                        onValueChange = { amount.value = it },

                        Modifier
                            .fillMaxWidth(.30f)
                            /*.shadow(5.dp, CircleShape)
                            .background(Color.White, CircleShape)*/
                            .padding(vertical = 0.dp),
                        singleLine = true,
                        maxLines = 1,
                        label = { Text("Amount (g)")}
                        )
                }

                Column(Modifier.padding(0.dp, 0.dp))
                {
                    Text("Date / Time of Event", Modifier.padding(10.dp, 0.dp), color = Color.DarkGray)
                    Row () {
                        DatePickerDialogBox(
                            onComplete = { date = it }
                        ) { openDialog ->
                            DateButton(date, onClick = openDialog )
                        }

                        TimePickerDialogBox(
                            onComplete = { time = it }
                        ) { openDialog ->
                            TimeButton(time, onClick = openDialog)
                        }
                    }
                }

            }
            Row(
                modifier = Modifier.padding(8.dp)
            ) {

                OutlinedTextField(
                    value = query.value,
                    onValueChange = { query.value = it},
                    modifier = Modifier
                        .fillMaxWidth()
                        /*.shadow(5.dp, CircleShape)
                        .background(Color.White, CircleShape)
                        .padding(horizontal = 15.dp, vertical = 10.dp),*/,
                    singleLine = true,
                    maxLines = 1,
                    label = { Text("Search for events") }
                    )

            }
            Row(
                modifier = Modifier
                    .padding(8.dp)
            ) {
                @Composable
                fun decideColor (type : EventChipType) : Color {
                    return (if (type == query_type.component1()) colorResource(id = R.color.selected_button_color) else colorResource(
                    id = R.color.purple_500)
                ) }
                CategoryChip(category = "All", selColor = decideColor(EventChipType.ALL), onChangeFun = { query_type.value = EventChipType.ALL } )
                CategoryChip(category = "Food", selColor = decideColor(EventChipType.FOOD), onChangeFun = { query_type.value = EventChipType.FOOD } )
                CategoryChip(category = "Exercise", selColor = decideColor(EventChipType.EXERCISE), onChangeFun = { query_type.value = EventChipType.EXERCISE } )
            }
            LazyColumn(modifier = Modifier
                .padding(8.dp)
                .fillMaxHeight(0.78f)
            ){
                val foodItems = foodItemList ?: listOf()
                val exercizeItems = exerciseItemList ?: listOf()

                items(foodItems) { food ->
                        var foodList: ArrayList<FoodItem> = arrayListOf()
                        if ( (query_type.component1() == EventChipType.ALL || query_type.component1() == EventChipType.FOOD) &&
                            (food.name.contains(query.component1(), ignoreCase = true) || query.component1().isEmpty()) ) {
                        FoodChip(
                            food,
                            EventChipType.FOOD,
                            onChangeFun = {
                                foodList = arrayListOf()
                                foodList.add(food)
                                eventModel.addEventItem(EventItem(foodItemList = foodList.toString(), time = time.atDate(date).toEpochSecond(ZoneOffset.of("Z"))/60 ))
                            }
                        )}
                    }

                items(exercizeItems) { exercise ->
                    var exerciseList: ArrayList<ExerciseItem> = arrayListOf()
                    if ((query_type.component1() == EventChipType.ALL || query_type.component1() == EventChipType.EXERCISE) &&
                        (exercise.name.contains(query.component1(), ignoreCase = true) || query.component1().isEmpty())) {
                        ExerciseChip(
                            exercise,
                            type = EventChipType.EXERCISE,
                            onChangeFun = {
                                exerciseList = arrayListOf()
                                exerciseList.add(exercise)
                                eventModel.addEventItem(EventItem(exerciseItemList = exerciseList.toString(), time = time.atDate(date).toEpochSecond(ZoneOffset.of("Z"))/60 ))}
                        )
                    }
                }
                }
            }
        }
}


@Composable
fun CategoryChip(category: String, selColor: Color, isSelected: Boolean = false, onChangeFun: () -> Unit){
    Surface (
            modifier = Modifier.padding(end = 8.dp),
            elevation = 8.dp,
            shape = MaterialTheme.shapes.medium,
            color = MaterialTheme.colors.primary
    ){
        Column(
                modifier = Modifier.padding(1.dp)
        ) {
            Row(
                    modifier = Modifier
                            .toggleable(
                                    value = isSelected,
                                    onValueChange = {}
                            )
            ) {
                Button(
                        colors = ButtonDefaults.buttonColors(backgroundColor = selColor),
                        onClick = onChangeFun
                ) { Text(category) }
            }

        }
    }
}

@Composable
fun FoodChip(foodItem: FoodItem, type: EventChipType, onChangeFun: () -> Unit){
    Surface (
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth(),
        elevation = 8.dp,
        shape = MaterialTheme.shapes.medium,
        color = Color.White
    ) {
        Row {
            Column(
                Modifier
                    .fillMaxWidth(0.8f)
            ) {
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = foodItem.name,
                    fontSize = 18.sp,
                )
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = "Serving Size: "+foodItem.servingSize.toString()+"g, RAG: "+foodItem.Rag.toString()+
                            "g, SAG: " + foodItem.Sag.toString() + "g, Protein: " + foodItem.Protein.toString()+
                            "g, Fat: "+ foodItem.Fat.toString()+ "g",
                        fontSize = 12.sp,
                )
            }
            Button(
                colors= ButtonDefaults.buttonColors(backgroundColor = Color.White),
                border = BorderStroke(2.dp, Color.White),
                elevation = ButtonDefaults.elevation(0.dp),
                onClick = onChangeFun,
                modifier = Modifier
                    .fillMaxWidth().align(Alignment.CenterVertically)
            ) { Text(text = "add") }
        }
    }
}
@Composable
fun ExerciseChip(exerciseItem: ExerciseItem, type: EventChipType, onChangeFun: () -> Unit){
    Surface (
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth(),
        elevation = 8.dp,
        shape = MaterialTheme.shapes.medium,
        color = Color.White
    ) {
        Row {
            Column(
                Modifier
                    .fillMaxWidth(0.8f)
            ) {
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = exerciseItem.name,
                    fontSize = 18.sp,
                )
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = "Intensity: "+exerciseItem.Intensity.toString()+" METs, Duration: "+exerciseItem.duration.toString(),
                    fontSize = 12.sp,
                )
            }
            Button(
                /*style = MaterialTheme.typography.body2,*/
                /*color = Color.White,*/
                colors= ButtonDefaults.buttonColors(backgroundColor = Color.White),
                border = BorderStroke(2.dp, Color.White),
                elevation = ButtonDefaults.elevation(0.dp),
                onClick = onChangeFun,
                modifier = Modifier
                    .fillMaxWidth().align(Alignment.CenterVertically)
            ) { Text("add") }
        }
    }
}

enum class EventChipType {
    ALL,
    FOOD,
    EXERCISE,
    ACTIVITY
}