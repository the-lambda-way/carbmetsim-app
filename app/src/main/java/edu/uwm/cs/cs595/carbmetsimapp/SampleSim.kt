package edu.uwm.cs.cs595.carbmetsimapp

import android.os.Bundle
import android.util.Log.d
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItem
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrAge
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrFitness
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrGender
import edu.uwm.cs.cs595.carbmetsimapp.MyApplication.Companion.usrWeight
import edu.uwm.cs.cs595.carbmetsimapp.databinding.SampleSimBinding
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.model.defaults.*

// See https://developer.android.com/guide/components/activities/intro-activities
@AndroidEntryPoint
class SampleSim : AppCompatActivity() {
    private lateinit var binding: SampleSimBinding

    private val exerciseItemViewModel: ExerciseItemViewModel by viewModels()
    private var exerciseItemList: List<ExerciseItem> = listOf()

    private val foodItemViewModel: FoodItemViewModel by viewModels()
    private var foodItemList: List<FoodItem> = listOf()

    private val eventItemViewModel: EventItemViewModel by viewModels()
    private var eventItemList: List<EventItem> = listOf()

    private var foodInput: HashMap<Int, FoodType> = hashMapOf()
    private var exerciseInput: HashMap<Int, ExerciseType> = hashMapOf()

    private var startTime: Long = 0
    private var endTime: Long = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sample_sim)
        val diabetic = intent.getBooleanExtra("DIABETIC",false)
//        startTime = intent.getLongExtra("startTime",0)
//        endTime = intent.getLongExtra("endTime",0)
        d("startTime", startTime.toString())
        d("endTime", endTime.toString())
        foodItemViewModel.readAllData.observe(this, { foodItems ->
            foodItemList=foodItems
        })

        eventItemViewModel.readAllData.observe(this, { eventItems ->
                eventItemList = eventItems
                if (!diabetic) {
                    exerciseItemViewModel.readAllData.observe(this, { exerciseItems ->
                        exerciseItemList=exerciseItems
                        //foodInput = getFoodInput()
                        //exerciseInput = getExerciseInput()
                        //findViewById<TextView>(R.id.sample_text).text = runNormalSample()
                    })

                }
                else {
                    findViewById<TextView>(R.id.sample_text).text = runDiabeticSample()
                }
        })
    }

    fun runDiabeticSample(): String {
        return "hi"
    }
    fun getFoodInput(foodItemList: List<FoodItem>?): HashMap<Int, FoodType> {
        val foodTypeHashMap = HashMap<Int, FoodType>()
        val foodList = foodItemList
        d("foodInput", foodList.toString())
        if (foodList != null) {
            foodList.forEach{
                val uid = it.uid
                val food = FoodType(it.uid, it.name, it.servingSize, it.Rag, it.Sag, it.Protein, it.Fat)

                foodTypeHashMap[uid] = food
                d("foodInput", foodTypeHashMap[uid].toString())
                d("foodInput", foodTypeHashMap.size.toString())
            }
        }
        return foodTypeHashMap
    }

    fun getExerciseInput(exerciseItemList: List<ExerciseItem>?): HashMap<Int, ExerciseType> {
        val exerciseTypeHashMap = HashMap<Int, ExerciseType>()
        val exerciseList = exerciseItemList
        d("exerciseInput", exerciseList.toString())
        if (exerciseList != null) {
            exerciseList.forEach {
                exerciseTypeHashMap[it.uid] =
                    ExerciseType(it.uid, it.name, it.Intensity)
                d("exerciseInput", exerciseTypeHashMap[it.uid].toString())
                d("exerciseInput", exerciseTypeHashMap.size.toString())
            }
        }
        return exerciseTypeHashMap
    }

    fun getFoodEvents(eventItemList: List<EventItem>?): ArrayList<FoodEvent> {
        val foodEventsList = arrayListOf<FoodEvent>()

        if (eventItemList != null) {
            eventItemList.forEach {
                val foodStr = it.foodItemList
                if(foodStr.length > 10) {
                    d("event", foodStr)
                    var delimiter1 = "[FoodItem("
                    var delimiter2 = "), FoodItem("
                    val delimiter3 = ")]"
                    val foodItems = foodStr.split(delimiter1, delimiter2, delimiter3)
                    foodItems.forEach { it1 ->
                        delimiter1 = "="
                        delimiter2 = ", "
                        val foodItemParams = it1.split(delimiter1, delimiter2).toTypedArray()
                        if (foodItemParams.size == 16) {
                            val foodTypeItem = FoodType(
                                foodItemParams[1].toInt(), foodItemParams[3],
                                foodItemParams[5].toDouble(), foodItemParams[7].toDouble(),
                                foodItemParams[9].toDouble(), foodItemParams[11].toDouble(),
                                foodItemParams[13].toDouble()
                            )
                            foodEventsList.add(
                                FoodEvent(
                                    (it.time-(1621555200)/60).toInt(),
                                    foodTypeItem.servingSize.toInt(),
                                    foodTypeItem.foodID
                                )
                            )
                        }
                    }
                }
            }
        }

        d("event", "done getting food events")
        return foodEventsList
    }

    fun getExerciseEvents(eventItemList: List<EventItem>?): ArrayList<ExerciseEvent> {
        val exerciseEventsList = arrayListOf<ExerciseEvent>()
        if (eventItemList != null) {
            eventItemList.forEach {
                val exerciseStr = it.exerciseItemList
                if(exerciseStr.length > 10) {
                    d("event", exerciseStr)
                    var delimiter1 = "[ExerciseItem("
                    var delimiter2 = "), ExerciseItem("
                    val delimiter3 = ")]"
                    val exerciseItems = exerciseStr.split(delimiter1, delimiter2, delimiter3)
                    exerciseItems.forEach { it1 ->
                        delimiter1 = "="
                        delimiter2 = ", "
                        val exerciseItemParams = it1.split(delimiter1, delimiter2).toTypedArray()

                        if (exerciseItemParams.size == 10) {
                            val exerciseTypeItem = ExerciseType(
                                exerciseItemParams[1].toInt(), exerciseItemParams[3],
                                exerciseItemParams[5].toDouble()
                            )
                            exerciseEventsList.add(
                                ExerciseEvent(
                                    (it.time-(1621555200)/60).toInt(),
                                    exerciseItemParams[7].toDouble().toInt(),
                                    exerciseTypeItem.exerciseID
                                )
                            )
                        }
                    }
                }
            }
        }
        d("event", "done getting exercise events")
        return exerciseEventsList
    }

    fun runNormalSample(foodInput: HashMap<Int, FoodType>, exerciseInput: HashMap<Int, ExerciseType>, exerciseEvents: ArrayList<ExerciseEvent>, foodEvents: ArrayList<FoodEvent>): ArrayList<Double> {

        val metabolicParams = run {
            val params = normalMetabolicParams(49, 89.0, 0, 50.0)
            hashMapOf(BodyState.FED_RESTING to params,
                    BodyState.FED_EXERCISING to params,
                    BodyState.POSTABSORPTIVE_RESTING to params,
                    BodyState.POSTABSORPTIVE_EXERCISING to params)
        }

        val sim = Simulator("alpha", metabolicParams, foodInput, exerciseInput)
        exerciseEvents.forEach {
            sim.addEvent(it)
        }

        foodEvents.forEach {
            sim.addEvent(it)
        }
        sim.addEvent(HaltEvent(1440))//(endTime-startTime).toInt()))

        var out: ArrayList<Double> = arrayListOf()
        while (sim.ticks() < 1440){//endTime-startTime) {
            sim.runTick()

            sim.update()
            //if (sim.ticks() % 60 == 0)
            out.add(sim.blood.getBGL)
        }


        sim.runTick()
        sim.update()

        return out
    }

    private fun timeStamp(sim: Simulator): String {
        return sim.elapsedDays().toString() + ":" + sim.elapsedHours() + ":" + sim.elapsedMinutes() +
                " " + sim.ticks() + " "
    }

    fun timelineSample() : ArrayList<Double> {
        val metabolicParams = run {
            val params = normalMetabolicParams(49, 89.0, 0, 50.0)
            hashMapOf(
                BodyState.FED_RESTING to params,
                BodyState.FED_EXERCISING to params,
                BodyState.POSTABSORPTIVE_RESTING to params,
                BodyState.POSTABSORPTIVE_EXERCISING to params
            )
        }

        val sim = Simulator("alpha", sampleMetabolicParams, sampleFoodInput, sampleExerciseInput)

        sim.addEvent(sampleFoodEvent)
        sim.addEvent(sampleExerciseEvent)
        //sim.addEvent(sampleFoodEvent1)

        sim.addEvent(HaltEvent(1440))
        var out: ArrayList<Double> = arrayListOf()
        while (sim.ticks() < 1440) {
            sim.runTick()

            sim.update()
            //if (sim.ticks() % 60 == 0)
                out.add(sim.blood.getBGL)
        }

        //val out = onBody(sim)
        sim.runTick()
        sim.update()

        return out
    }

    companion object {
        // Used to load the 'carbmetlib' library on application startup.
        init {
            System.loadLibrary("carbmetlib")
        }
    }
}