package edu.uwm.cs.cs595.carbmetsimapp

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItem
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.databinding.EventManagerBinding
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import java.lang.reflect.Array
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class EventManager: AppCompatActivity(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{
    private lateinit var binding: EventManagerBinding

    private val exerciseItemViewModel: ExerciseItemViewModel by viewModels()
    private val exerciseItemAdapter: ExerciseItemAdapter by lazy { ExerciseItemAdapter() }

    private val foodItemViewModel: FoodItemViewModel by viewModels()
    private val foodItemAdapter: FoodItemAdapter by lazy { FoodItemAdapter() }

    private val eventItemViewModel: EventItemViewModel by viewModels()
    private val eventItemAdapter: EventItemAdapter by lazy { EventItemAdapter() }

    private var date: Long = 0

    var day = 0
    var month = 0
    var year = 0
    var hour = 0
    var minute = 0

    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0
    var savedHour = 0
    var savedMinute = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EventManagerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        pickDate()

        binding.allExercises.layoutManager = LinearLayoutManager(this)
        binding.allExercises.adapter = exerciseItemAdapter
        exerciseItemViewModel.readAllData.observe(this, {
            exerciseItemAdapter.setData(it)
        })

        binding.allFoods.layoutManager = LinearLayoutManager(this)
        binding.allFoods.adapter = foodItemAdapter
        foodItemViewModel.readAllData.observe(this, {
            foodItemAdapter.setData(it)
        })

        binding.newExerciseButton.setOnClickListener{
            startActivity(Intent(this, NewExercise::class.java))
        }
        binding.newFoodButton.setOnClickListener{
            startActivity(Intent(this, NewFood::class.java))
        }
        binding.addEvent.setOnClickListener{
            val foodItemList: ArrayList<FoodItem> = arrayListOf()
            foodItemAdapter.foodItemList.forEach{
                if(it.checked)
                    foodItemList.add(it)
            }
            val exerciseItemList: ArrayList<ExerciseItem> = arrayListOf()
            exerciseItemAdapter.exerciseItemList.forEach{
                if(it.checked)
                    exerciseItemList.add(it)
            }
            val time = binding.EventTime.text.toString()
            if(time == "" || (foodItemList.size == 0  && exerciseItemList.size == 0)){
                Toast.makeText(applicationContext, "Please enter all data", Toast.LENGTH_LONG).show()
            }
            else{
                val eventItem = EventItem(0, foodItemList.toString(), exerciseItemList.toString(), date)
                eventItemViewModel.addEventItem(eventItem)
                eventItemViewModel.readAllData.observe(this, {
                    eventItemAdapter.setData(it)
                })
                startActivity(Intent(this, RunSim::class.java))
            }

        }


    }

    private fun pickDate(){
        binding.EventTime.setOnClickListener{
            getDateTimeCalendar()

            DatePickerDialog(this, this, year, month, day).show()
        }
    }

    private fun getDateTimeCalendar() {
        val cal = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
        hour = cal.get(Calendar.HOUR)
        minute = cal.get(Calendar.MINUTE)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month
        savedYear = year

        getDateTimeCalendar()
        TimePickerDialog(this, this, hour, minute, true).show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        savedHour = hourOfDay
        savedMinute = minute
        var timeDate = ""
        if(savedMinute == 0)
            timeDate = "$savedMonth-$savedDay-$savedYear  $savedHour:00"
        else
            timeDate = "$savedMonth-$savedDay-$savedYear  $savedHour:$savedMinute"
        binding.EventTime.setText(timeDate)
        val calDate = Calendar.getInstance()
        calDate.clear()
        calDate.set(savedYear, savedMonth, savedDay, savedHour, savedMinute)
        date = calDate.timeInMillis/60000
    }
}