package edu.uwm.cs.cs595.carbmetsimapp


class Simulator(
    val seedString:          String,
    val metabolicParameters: HashMap<BodyState, MetabolicParams>,
    val foodTypes:           HashMap<Int, FoodType>,
    val exerciseTypes:       HashMap<Int, ExerciseType>
) {
    external fun addEvent(fireTime: Int, type: EventType, id: Int, howmuch: Int)
    external fun addEvent(event: Event)

    external fun runTick(): Boolean
    external fun runToHalt()

    external fun eventsWereFired(): Boolean
    external fun getFiredEvents(): ArrayList<Event>

    external fun elapsedDays():    Int
    external fun elapsedHours():   Int
    external fun elapsedMinutes(): Int
    external fun ticks():          Int

    external fun dayOver(): Boolean

    // Interface to body and organ parameters
    val blood:         Blood         = Blood()
    val body:          HumanBody     = HumanBody()
    val brain:         Brain         = Brain()
    val heart:         Heart         = Heart()
    val intestine:     Intestine     = Intestine()
    val kidneys:       Kidneys       = Kidneys()
    val liver:         Liver         = Liver()
    val muscles:       Muscles       = Muscles()
    val portalVein:    PortalVein    = PortalVein()
    val stomach:       Stomach       = Stomach()

    external fun update()
    external fun nativeDestruct()

    private val nativeHandle: Long = 0
    private external fun nativeInit()

    init {
        nativeInit()
        update()
    }

    companion object
    {
        external fun timeToTicks(days: Int, hours: Int, minutes: Int): Int

        /*
         * We use a static class initializer to allow the native code to cache some
         * field offsets. This native function looks up and caches interesting
         * class/field/method IDs. Throws on failure.
         */
        private external fun onClassInit()

        init {
            onClassInit()
        }
    }
}