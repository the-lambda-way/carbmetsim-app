package edu.uwm.cs.cs595.carbmetsimapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.databinding.NewFoodBinding
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem

@AndroidEntryPoint
class NewFood: AppCompatActivity(){

    private lateinit var binding: NewFoodBinding
    private val foodItemViewModel: FoodItemViewModel by viewModels()
    private val foodItemAdapter: FoodItemAdapter by lazy { FoodItemAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = NewFoodBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.addFoodButton.setOnClickListener {
            val foodName      = binding.foodName.text.toString()
            val servingSize = binding.foodServingSize.text.toString().toDouble()
            val rag = binding.foodRAG.text.toString().toDouble()
            val sag = binding.foodSAG.text.toString().toDouble()
            val protein = binding.foodProtein.text.toString().toDouble()
            val fat = binding.foodFat.text.toString().toDouble()
            val foodItem = FoodItem(0, foodName, servingSize, rag, sag, protein, fat)
            foodItemViewModel.addFoodItem(foodItem)
            foodItemViewModel.readAllData.observe(this, {
                foodItemAdapter.setData(it)
            })
            startActivity(Intent(this, EventManager::class.java))
        }
    }
}




