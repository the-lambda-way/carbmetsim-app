package edu.uwm.cs.cs595.carbmetsimapp.model.defaults

import edu.uwm.cs.cs595.carbmetsimapp.*


// Derived from ParamsNormal.txt from the meal-event-normal system test.
fun normalMetabolicParams(age: Int, weight: Double, gender: Int, fitness: Double): MetabolicParams
{
    val muscles = MusclesParams(
        glycogen                        = 500.0,
        glycogenMax                     = 500.0,
        Glut4VMAX                       = 3.5,
        peakGlut4VMAX                   = 7.0,
        glucoseToGlycogen               = 7.0,
        glycogenShareDuringExerciseMean = 0.53,
    )

    val liver = LiverParams(
        glycogen                 = 100.0,
        glycogenMax              = 120.0,
        glucoseToGlycogenInLiver = 4.5,
        glycogenToGlucoseInLiver = 0.9,
        gngLiver                 = 0.16,
        maxLipogenesis           = 400.0,
    )

    val kidneys = KidneysParams(
        gngKidneys = 0.16,
    )

    val body = HumanParams(
        gngImpact                                    = 6.0,
        liverGlycogenBreakdownImpact                 = 6.0,
        intensityPeakGlucoseProd                     = 0.2,
        glut4Impact                                  = 1.0,
        glycolysisMinImpact                          = 1.0,
        glycolysisMaxImpact                          = 1.0,
        excretionKidneysImpact                       = 1.0,
        liverGlycogenSynthesisImpact                 = 1.0,
        insulinImpactOnGNG_Mean                      = 0.5,
        insulinImpactOnGNG_StdDev                    = 0.2,
        insulinImpactGlycogenBreakdownInLiver_Mean   = 0.1,
        insulinImpactGlycogenBreakdownInLiver_StdDev = 0.02,
        insulinImpactGlycogenSynthesisInLiver_Mean   = 0.5,
        insulinImpactGlycogenSynthesisInLiver_StdDev = 0.2,
        insulinImpactOnGlycolysis_Mean               = 0.5,
        insulinImpactOnGlycolysis_StdDev             = 0.2,
        bodyWeight                                   = weight,
        age                                          = age.toDouble(),
        gender                                       = gender.toDouble(),
        fitnessLevel                                 = fitness,
    )

    val blood = BloodParams(
        minGlucoseLevel  = 50.0,
        baseGlucoseLevel = 90.0,
        highGlucoseLevel = 145.0,
        baseInsulinLevel = 0.001,
        peakInsulinLevel = 1.0,
    )

    return MetabolicParams(muscles = muscles, liver = liver, kidneys = kidneys, body = body, blood = blood)
}


// Derived from ParamsDiab.txt from the meal-event-diabetic system test.
fun diabeticMetabolicParams(age: Int, weight: Double, gender: Int, fitness: Double): MetabolicParams
{
    val muscles = MusclesParams(
        glycogen                        = 500.0,
        glycogenMax                     = 500.0,
        Glut4VMAX                       = 3.5,
        peakGlut4VMAX                   = 7.0,
        glucoseToGlycogen               = 7.0,
        glycogenShareDuringExerciseMean = 0.53,
    )

    val liver = LiverParams(
        glycogen                 = 100.0,
        glycogenMax              = 120.0,
        glucoseToGlycogenInLiver = 6.75,
        glycogenToGlucoseInLiver = 1.25,
        gngLiver                 = 0.38,
        maxLipogenesis           = 400.0,
    )

    val kidneys = KidneysParams(
        gngKidneys = 0.38,
    )

    val body = HumanParams(
        gngImpact                                    = 6.0,
        liverGlycogenBreakdownImpact                 = 6.0,
        intensityPeakGlucoseProd                     = 0.2,
        glut4Impact                                  = 0.25,
        glycolysisMinImpact                          = 4.0,
        glycolysisMaxImpact                          = 1.5,
        excretionKidneysImpact                       = 1.3,
        liverGlycogenSynthesisImpact                 = 1.0,
        insulinImpactOnGNG_Mean                      = 0.5,
        insulinImpactOnGNG_StdDev                    = 0.2,
        insulinImpactGlycogenBreakdownInLiver_Mean   = 0.1,
        insulinImpactGlycogenBreakdownInLiver_StdDev = 0.02,
        insulinImpactGlycogenSynthesisInLiver_Mean   = 0.5,
        insulinImpactGlycogenSynthesisInLiver_StdDev = 0.2,
        insulinImpactOnGlycolysis_Mean               = 0.5,
        insulinImpactOnGlycolysis_StdDev             = 0.2,
        bodyWeight                                   = weight,
        age                                          = age.toDouble(),
        gender                                       = gender.toDouble(),
        fitnessLevel                                 = fitness,
    )

    val blood = BloodParams(
        minGlucoseLevel  = 50.0,
        baseGlucoseLevel = 210.0,
        highGlucoseLevel = 360.0,
        baseInsulinLevel = 0.001,
        peakInsulinLevel = 0.6,
    )

    return MetabolicParams(muscles = muscles, liver = liver, kidneys = kidneys, body = body, blood = blood)
}


// Derived from Food.txt from the meal-event-normal system test
val sampleFood = FoodType(1, "Breakfast", 120.0, 84.0, 0.0, 26.0, 10.0)
val sampleFood1 = FoodType(2, "lunch", 200.0, 100.0, 0.0, 32.0, 14.0)


// Derived from Events.txt from the meal-event-normal system test
val sampleFoodEvent = FoodEvent(10 * 60, 120, 1)
val sampleFoodEvent1 = FoodEvent( 14 * 60, 200, 2)


// Derived from ExerciseLeg30_1.txt from the exercise-event-AhlborgLeg30 system test
val sampleExercise = ExerciseType(1, "Walk", 4.114)


// Derived from Events30.txt from the exercise-event-AhlborgLeg30 system test
val sampleExerciseEvent = ExerciseEvent(12 * 60, 120, 1)


// Derived from Events.txt from the meal-event-normal system test
val sampleHaltEvent = HaltEvent(18 * 60)


val sampleMetabolicParams = run {
    val params = normalMetabolicParams(49, 89.0, 0, 50.0)
    hashMapOf(BodyState.FED_RESTING               to params,
              BodyState.FED_EXERCISING            to params,
              BodyState.POSTABSORPTIVE_RESTING    to params,
              BodyState.POSTABSORPTIVE_EXERCISING to params)
}
val sampleFoodInput     = hashMapOf(1 to sampleFood, 2 to sampleFood1)
val sampleExerciseInput = hashMapOf(1 to sampleExercise)
val sampleEventsInput   = arrayOf(sampleFoodEvent, sampleFoodEvent1, sampleExerciseEvent, sampleHaltEvent)
