package edu.uwm.cs.cs595.carbmetsimapp.ui.components

import androidx.annotation.StringRes
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import edu.uwm.cs.cs595.carbmetsimapp.R


@Composable
fun AppTopAppBar(@StringRes title: Int, onBackButton: () -> Unit) {
    TopAppBar(
        title          = { Text(stringResource(title)) },
        navigationIcon = { BackButton(onBackButton) }
    )
}

@Preview
@Composable
fun AppTopAppBarPreview() {
    AppTopAppBar(R.string.timeline) {}
}


@Composable
fun AppDialogTopAppBar(
    @StringRes title: Int,
    onCloseButton: () -> Unit,
    onSaveButton: () -> Unit,
    saveEnabled: Boolean = false,
) {
    TopAppBar(
        title          = { Text(stringResource(title)) },
        navigationIcon = { CloseButton(onCloseButton) },
        actions        = { SaveButton(enabled = saveEnabled) { onSaveButton(); onCloseButton() } }
    )
}

@Preview
@Composable
fun AppDialogTopAppBarSaveDisabledPreview() {
    AppDialogTopAppBar(
        title         = R.string.editItem,
        onCloseButton = {},
        onSaveButton  = {}
    )
}

@Preview
@Composable
fun AppDialogTopAppBarSaveEnabledPreview() {
    AppDialogTopAppBar(
        title         = R.string.editItem,
        onCloseButton = {},
        onSaveButton  = {},
        saveEnabled   = true,
    )
}
