package edu.uwm.cs.cs595.carbmetsimapp.EventItem

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

class EventItemViewModel @ViewModelInject constructor(
    private val repository: EventItemRepository
): ViewModel(){

    val readAllData = repository.readAllData().asLiveData()

    fun addEventItem(eventItem: EventItem){
        viewModelScope.launch (Dispatchers.IO){
            repository.addEventItem(eventItem)
        }
    }

}
