package edu.uwm.cs.cs595.carbmetsimapp.EventItem

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.uwm.cs.cs595.carbmetsimapp.databinding.EventDesignBinding
import edu.uwm.cs.cs595.carbmetsimapp.databinding.EventItemDesignBinding

class EventItemAdapter: RecyclerView.Adapter<EventItemAdapter.MyViewHolder>(){

    var eventItemList = emptyList<EventItem>()

    class MyViewHolder(val binding: EventItemDesignBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            EventItemDesignBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val id = "Event ${eventItemList[position].uid.toString()}"
        val exerciseItems = eventItemList[position].exerciseItemList.toString()
        val foodItems = eventItemList[position].foodItemList.toString()
        val time = eventItemList[position].time.toString()
        val details = "Time: $time\nFoods: $foodItems\nExercises:$exerciseItems"
        holder.binding.eventNumber.text = id
        holder.binding.eventDetails.text = details
    }

    override fun getItemCount(): Int {
        return eventItemList.size
    }

    fun setData(eventItem: List<EventItem>){
        eventItemList = eventItem
        notifyDataSetChanged()
    }
}