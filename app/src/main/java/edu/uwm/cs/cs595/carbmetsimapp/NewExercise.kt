package edu.uwm.cs.cs595.carbmetsimapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.databinding.NewExerciseBinding
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItem

@AndroidEntryPoint
class NewExercise: AppCompatActivity(){

    private lateinit var binding: NewExerciseBinding
    private val exerciseItemViewModel: ExerciseItemViewModel by viewModels()
    private val exerciseItemAdapter: ExerciseItemAdapter by lazy { ExerciseItemAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = NewExerciseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.addExerciseButton.setOnClickListener {
            val exerciseName = binding.exerciseName.text.toString()
            val exerciseIntensity = binding.exerciseIntensity.text.toString().toDouble()
            val exerciseDuration = binding.exerciseDuration.text.toString().toDouble()
            val exerciseItem = ExerciseItem(0, exerciseName, exerciseIntensity, exerciseDuration)
            exerciseItemViewModel.addExerciseItem(exerciseItem)
            exerciseItemViewModel.readAllData.observe(this, {
                exerciseItemAdapter.setData(it)
            })
            startActivity(Intent(this, EventManager::class.java))
        }
    }
}






