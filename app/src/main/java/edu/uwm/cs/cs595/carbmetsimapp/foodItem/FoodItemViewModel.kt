package edu.uwm.cs.cs595.carbmetsimapp.foodItem

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


class FoodItemViewModel @ViewModelInject constructor(
    private val repository: FoodItemRepository
): ViewModel(){

    val readAllData = repository.readAllData().asLiveData()

    fun addFoodItem(foodItem: FoodItem){
        viewModelScope.launch (Dispatchers.IO){
            repository.addFoodItem(foodItem)
        }
    }

}
