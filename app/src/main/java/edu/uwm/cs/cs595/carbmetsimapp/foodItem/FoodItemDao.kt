package edu.uwm.cs.cs595.carbmetsimapp.foodItem

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface FoodItemDao {
    @Query("SELECT * FROM food_item_table ORDER BY uid ASC")
    fun readAllData(): Flow<List<FoodItem>>
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFoodItem(foodItem: FoodItem)
    @Query("SELECT * From food_item_table WHERE name = :foodName")
    fun getFoodItem(foodName: String): FoodItem
}
