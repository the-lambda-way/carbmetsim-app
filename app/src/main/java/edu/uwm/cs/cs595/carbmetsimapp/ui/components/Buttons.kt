package edu.uwm.cs.cs595.carbmetsimapp.ui.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.material.ButtonDefaults.textButtonColors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.toLowerCase
import androidx.compose.ui.tooling.preview.Preview
import edu.uwm.cs.cs595.carbmetsimapp.R
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter


@Composable
fun ExpandButton(onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(Icons.Default.ExpandMore, stringResource(R.string.expand))
    }
}

@Preview
@Composable
fun ExpandButtonPreview() {
    ExpandButton {}
}


@Composable
fun CollapseButton(onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(Icons.Default.ExpandLess, stringResource(R.string.collapse))
    }
}

@Preview
@Composable
fun CollapseButtonPreview() {
    CollapseButton {}
}


@Composable
fun BackButton(onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(Icons.Default.ChevronLeft, stringResource(R.string.back))
    }
}

@Preview
@Composable
fun BackButtonPreview() {
    BackButton {}
}


@Composable
fun CloseButton(onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(Icons.Default.Close, stringResource(R.string.close))
    }
}

@Preview
@Composable
fun CloseButtonPreview() {
    CloseButton {}
}


@Composable
fun SaveButton(enabled: Boolean = false, onClick: () -> Unit) {
    TextButton(
        onClick = onClick,
        colors  = textButtonColors(contentColor = contentColorFor(MaterialTheme.colors.primarySurface)),
        enabled = enabled,
    ) {
        Text(stringResource(R.string.save))
    }
}

@Preview
@Composable
fun SaveButtonDisabledPreview() {
    SaveButton {}
}

@Preview
@Composable
fun SaveButtonEnabledPreview() {
    SaveButton(enabled = true) {}
}


@Composable
fun DateButton(
    date: LocalDate,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val format        = remember { DateTimeFormatter.ofPattern("M/d/yyyy") }
    val formattedDate = remember(date) { date.format(format) }

    TextButton(onClick, modifier) {
        Icon(
            imageVector        = Icons.Default.Event,
            contentDescription = stringResource(R.string.datePicker),
            modifier           = modifier
        )
        Spacer(Modifier.size(ButtonDefaults.IconSpacing))
        Text(formattedDate)
    }
}

@Preview
@Composable
fun DateButtonPreview() {
    DateButton(LocalDate.now(), onClick = {})
}


@Composable
fun TimeButton(
    time: LocalTime,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val format        = remember { DateTimeFormatter.ofPattern("h:mm a") }
    val formattedTime = remember(time) { time.format(format).toLowerCase(Locale.current) }

    TextButton(onClick, modifier) {
        Icon(
            imageVector        = Icons.Default.Schedule,
            contentDescription = stringResource(R.string.timePicker),
            modifier           = modifier
        )
        Spacer(Modifier.size(ButtonDefaults.IconSpacing))
        Text(formattedTime)
    }
}

@Preview
@Composable
fun TimeButtonPreview() {
    TimeButton(LocalTime.now(), onClick = {})
}
