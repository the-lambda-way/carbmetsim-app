package edu.uwm.cs.cs595.carbmetsimapp

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.EventItem.EventItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.databinding.RunSimBinding
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItem
import android.util.Log.d
import android.widget.DatePicker
import android.widget.TimePicker
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.exerciseItem.ExerciseItemViewModel
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemAdapter
import edu.uwm.cs.cs595.carbmetsimapp.foodItem.FoodItemViewModel
import java.util.*


@AndroidEntryPoint
class RunSim: AppCompatActivity(), DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{

    private lateinit var binding: RunSimBinding

    private val eventItemViewModel: EventItemViewModel by viewModels()
    private val eventItemAdapter: EventItemAdapter by lazy { EventItemAdapter() }

    private var date1: Long = 0
    private var date2: Long = 0

    var day = 0
    var month = 0
    var year = 0
    var hour = 0
    var minute = 0

    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0
    var savedHour = 0
    var savedMinute = 0

    var startEnd = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = RunSimBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //val name = intent.getStringExtra("NAME")

        binding.events.layoutManager = LinearLayoutManager(this)
        binding.events.adapter = eventItemAdapter
        eventItemViewModel.readAllData.observe(this, {
            eventItemAdapter.setData(it)
        })

        pickDate()

        binding.addEvents.setOnClickListener{
            startActivity(Intent(this, EventManager::class.java))
        }

        binding.runSim.setOnClickListener {
            if (binding.diabetic.isChecked) {
                startActivity(Intent(this, SampleSim::class.java).apply { putExtra("DIABETIC", true) })
            }
            else{

                startActivity(Intent(this, SampleSim::class.java).apply {
                    putExtra("DIABETIC", false)
                    putExtra("startTime", date1)
                    putExtra("endTime", date2)
                })
            }

        }
    }
    private fun pickDate(){
        binding.startTime.setOnClickListener{
            getDateTimeCalendar()
            startEnd = 1
            DatePickerDialog(this, this, year, month, day).show()
        }
        binding.endTime.setOnClickListener{
            getDateTimeCalendar()
            startEnd = 2
            DatePickerDialog(this, this, year, month, day).show()
        }
    }

    private fun getDateTimeCalendar() {
        val cal = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
        hour = cal.get(Calendar.HOUR)
        minute = cal.get(Calendar.MINUTE)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay = dayOfMonth
        savedMonth = month
        savedYear = year

        getDateTimeCalendar()
        TimePickerDialog(this, this, hour, minute, true).show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        savedHour = hourOfDay
        savedMinute = minute
        var timeDate = ""
        if(savedMinute == 0)
            timeDate = "$savedMonth-$savedDay-$savedYear  $savedHour:00"
        else
            timeDate = "$savedMonth-$savedDay-$savedYear  $savedHour:$savedMinute"
        val calDate = Calendar.getInstance()
        calDate.clear()
        calDate.set(savedYear, savedMonth, savedDay, savedHour, savedMinute)
        if (startEnd == 1){
            date1 = calDate.timeInMillis/60000
            binding.startTime.setText(timeDate)
        }
        else if (startEnd == 2){
            date2 = calDate.timeInMillis/60000
            binding.endTime.setText(timeDate)
        }

    }
}