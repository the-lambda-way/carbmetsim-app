package edu.uwm.cs.cs595.carbmetsimapp.foodItem

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FoodItemDatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        FoodItemDatabase::class.java,
        "food_item_database"
    ).build()

    @Singleton
    @Provides
    fun provideDao(database: FoodItemDatabase) = database.foodItemDao()

}