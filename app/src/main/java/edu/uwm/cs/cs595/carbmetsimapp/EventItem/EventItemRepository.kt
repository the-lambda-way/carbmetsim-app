package edu.uwm.cs.cs595.carbmetsimapp.EventItem

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class EventItemRepository @Inject constructor(
    private val eventItemDao: EventItemDao
) {

   //val readAllData: Flow<List<ExerciseItem>> = exerciseItemDao.readAllData()
    fun readAllData(): Flow<List<EventItem>> {
        return eventItemDao.readAllData()
    }
    suspend fun addEventItem(eventItem: EventItem){
        eventItemDao.addEventItem(eventItem)
    }
}