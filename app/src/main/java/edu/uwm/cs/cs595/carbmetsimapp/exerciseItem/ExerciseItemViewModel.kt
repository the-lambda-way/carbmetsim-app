package edu.uwm.cs.cs595.carbmetsimapp.exerciseItem

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

class ExerciseItemViewModel @ViewModelInject constructor(
    private val repository: ExerciseItemRepository
): ViewModel(){

    val readAllData = repository.readAllData().asLiveData()

    fun addExerciseItem(exerciseItem: ExerciseItem){
        viewModelScope.launch (Dispatchers.IO){
            repository.addExerciseItem(exerciseItem)
        }
    }

}
