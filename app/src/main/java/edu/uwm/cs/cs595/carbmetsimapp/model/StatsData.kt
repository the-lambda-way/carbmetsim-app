package edu.uwm.cs.cs595.carbmetsimapp.model
import androidx.room.DatabaseView
import java.time.LocalDateTime
import java.time.LocalDate

//DatabaseView("select max(bgl) as max, min(bgl) as min, sum(bgl) / count(*) as average from bgl_history")
data class StatsData(
    val max : Double,
    val min : Double,
    val average : Double,
)

interface BeaglStatsConnector {
    fun getAllTimeStats( ) : StatsData
    fun getDailyStats( day : LocalDate ) : StatsData
    fun getWeeklyStats( week : LocalDate ) : StatsData
    fun getMonthlyStats( month : LocalDate ) : StatsData
}

class DummyBeaglStats : BeaglStatsConnector {
    override fun getAllTimeStats(): StatsData {
        return StatsData( 150.toDouble(), 75.toDouble(), 120.toDouble() )
    }

    override fun getDailyStats(day: LocalDate): StatsData {
        return StatsData( 150.toDouble() + (day.dayOfMonth).toDouble(), (100).toDouble() - day.dayOfMonth.toDouble(), 165.toDouble() - day.monthValue.toDouble())
    }

    override fun getMonthlyStats(month: LocalDate): StatsData {
        return StatsData( 150.toDouble() + (month.monthValue).toDouble(), (100).toDouble() - month.monthValue.toDouble(), 165.toDouble() - month.monthValue.toDouble())
    }

    override fun getWeeklyStats(week: LocalDate): StatsData {
        return StatsData( 400.toDouble() + (week.dayOfMonth).toDouble(), (100).toDouble() - week.dayOfMonth.toDouble(), 165.toDouble() - week.monthValue.toDouble())
    }
}